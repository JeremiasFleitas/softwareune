package sistema;

import clasesIniciales.Docente;
import clasesIniciales.Especialidad;
import conexiones.ConexionAlumnos;
import javax.swing.table.DefaultTableModel;
import com.mysql.jdbc.Connection;
import conexiones.Conexion;
import conexiones.ConexionAcreditacionesDocentes;
import conexiones.ConexionAreaAcreditaciones;
import conexiones.ConexionDocentes;
import java.awt.geom.Area;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.*;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author Fernando, Jeremias
 */
public class ABM_CV_Docentes extends javax.swing.JFrame {

    public ABM_CV_Docentes() {
        initComponents();
        cargarDatos();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        opciones_popupMenu = new javax.swing.JPopupMenu();
        menuIt_editarTitulo = new javax.swing.JMenuItem();
        menuIt_borrarTitulo = new javax.swing.JMenuItem();
        opcionesArea_popupMenu = new javax.swing.JPopupMenu();
        menuIt_editarArea = new javax.swing.JMenuItem();
        menuIt_borrarArea = new javax.swing.JMenuItem();
        panel_asignarTitulo = new javax.swing.JPanel();
        labelD_titulo = new javax.swing.JLabel();
        labelD_area = new javax.swing.JLabel();
        txtF_titulo = new javax.swing.JTextField();
        labelT_asignarTitulo = new javax.swing.JLabel();
        labelD_docente = new javax.swing.JLabel();
        comboB_docente = new javax.swing.JComboBox<>();
        comboB_area = new javax.swing.JComboBox<>();
        btn_guardarTitulo = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        separator1 = new javax.swing.JSeparator();
        btn_editarTitulo = new javax.swing.JButton();
        panel_tabladocente = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_AreaAcreditacion = new javax.swing.JTable();
        panel_buscarDocente = new javax.swing.JPanel();
        btn_buscar = new javax.swing.JButton();
        comboB_busqueda = new javax.swing.JComboBox<>();
        txtF_busqueda = new javax.swing.JTextField();
        labelT_buscar = new javax.swing.JLabel();
        separator3 = new javax.swing.JSeparator();
        panel_area = new javax.swing.JPanel();
        labelT_area = new javax.swing.JLabel();
        labelD_nombre = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        table_area = new javax.swing.JTable();
        txtF_nombreArea = new javax.swing.JTextField();
        btn_buscarArea = new javax.swing.JButton();
        btn_guardarArea = new javax.swing.JButton();
        btn_cancelarArea = new javax.swing.JButton();
        separator2 = new javax.swing.JSeparator();
        btn_editarArea = new javax.swing.JButton();

        menuIt_editarTitulo.setText("Editar");
        menuIt_editarTitulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_editarTituloActionPerformed(evt);
            }
        });
        opciones_popupMenu.add(menuIt_editarTitulo);

        menuIt_borrarTitulo.setText("Borrar");
        menuIt_borrarTitulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_borrarTituloActionPerformed(evt);
            }
        });
        opciones_popupMenu.add(menuIt_borrarTitulo);

        menuIt_editarArea.setText("Editar");
        menuIt_editarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_editarAreaActionPerformed(evt);
            }
        });
        opcionesArea_popupMenu.add(menuIt_editarArea);

        menuIt_borrarArea.setText("Borrar");
        menuIt_borrarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_borrarAreaActionPerformed(evt);
            }
        });
        opcionesArea_popupMenu.add(menuIt_borrarArea);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        panel_asignarTitulo.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        labelD_titulo.setText("Título:");

        labelD_area.setText("Área:");

        txtF_titulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtF_tituloActionPerformed(evt);
            }
        });

        labelT_asignarTitulo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelT_asignarTitulo.setText("Asignar titulos");

        labelD_docente.setText("Docente:");

        comboB_docente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboB_docenteActionPerformed(evt);
            }
        });

        btn_guardarTitulo.setText("Guardar");
        btn_guardarTitulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarTituloActionPerformed(evt);
            }
        });

        btn_cancelar.setText("Cancelar");
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        btn_editarTitulo.setText("Editar");
        btn_editarTitulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarTituloActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_asignarTituloLayout = new javax.swing.GroupLayout(panel_asignarTitulo);
        panel_asignarTitulo.setLayout(panel_asignarTituloLayout);
        panel_asignarTituloLayout.setHorizontalGroup(
            panel_asignarTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_asignarTituloLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_asignarTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_asignarTituloLayout.createSequentialGroup()
                        .addComponent(labelT_asignarTitulo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(separator1, javax.swing.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_asignarTituloLayout.createSequentialGroup()
                        .addGroup(panel_asignarTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_asignarTituloLayout.createSequentialGroup()
                                .addGroup(panel_asignarTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelD_area)
                                    .addComponent(labelD_titulo)
                                    .addGroup(panel_asignarTituloLayout.createSequentialGroup()
                                        .addComponent(labelD_docente)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(panel_asignarTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(comboB_area, 0, 371, Short.MAX_VALUE)
                                            .addComponent(txtF_titulo)
                                            .addComponent(comboB_docente, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_asignarTituloLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(panel_asignarTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btn_cancelar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn_guardarTitulo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn_editarTitulo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap())))
        );
        panel_asignarTituloLayout.setVerticalGroup(
            panel_asignarTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_asignarTituloLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_asignarTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelT_asignarTitulo)
                    .addComponent(separator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_asignarTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelD_docente)
                    .addComponent(comboB_docente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(panel_asignarTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelD_titulo)
                    .addComponent(txtF_titulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(panel_asignarTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelD_area)
                    .addComponent(comboB_area, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(btn_guardarTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_editarTitulo)
                .addGap(11, 11, 11)
                .addComponent(btn_cancelar)
                .addGap(6, 6, 6))
        );

        panel_tabladocente.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        table_AreaAcreditacion.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        table_AreaAcreditacion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Contador", "Docente", "Tituto", "Area"
            }
        ));
        table_AreaAcreditacion.setComponentPopupMenu(opciones_popupMenu);
        jScrollPane2.setViewportView(table_AreaAcreditacion);
        table_AreaAcreditacion.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout panel_tabladocenteLayout = new javax.swing.GroupLayout(panel_tabladocente);
        panel_tabladocente.setLayout(panel_tabladocenteLayout);
        panel_tabladocenteLayout.setHorizontalGroup(
            panel_tabladocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        panel_tabladocenteLayout.setVerticalGroup(
            panel_tabladocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_tabladocenteLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panel_buscarDocente.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        comboB_busqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Docente", "Titulo", "Area" }));
        comboB_busqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboB_busquedaActionPerformed(evt);
            }
        });

        txtF_busqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtF_busquedaActionPerformed(evt);
            }
        });

        labelT_buscar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelT_buscar.setText("Buscar");

        javax.swing.GroupLayout panel_buscarDocenteLayout = new javax.swing.GroupLayout(panel_buscarDocente);
        panel_buscarDocente.setLayout(panel_buscarDocenteLayout);
        panel_buscarDocenteLayout.setHorizontalGroup(
            panel_buscarDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_buscarDocenteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_buscarDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_buscarDocenteLayout.createSequentialGroup()
                        .addComponent(comboB_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtF_busqueda)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_buscar)
                        .addGap(34, 34, 34))
                    .addGroup(panel_buscarDocenteLayout.createSequentialGroup()
                        .addComponent(labelT_buscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(separator3))))
        );
        panel_buscarDocenteLayout.setVerticalGroup(
            panel_buscarDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_buscarDocenteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_buscarDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelT_buscar)
                    .addComponent(separator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(panel_buscarDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_buscar)
                    .addComponent(comboB_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtF_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        panel_area.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        labelT_area.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelT_area.setText("Areas");

        labelD_nombre.setText("Nombre:");

        table_area.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Contador", "Nombre"
            }
        ));
        table_area.setComponentPopupMenu(opcionesArea_popupMenu);
        jScrollPane3.setViewportView(table_area);

        txtF_nombreArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtF_nombreAreaActionPerformed(evt);
            }
        });

        btn_buscarArea.setText("Buscar");
        btn_buscarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarAreaActionPerformed(evt);
            }
        });

        btn_guardarArea.setText("Guardar");
        btn_guardarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarAreaActionPerformed(evt);
            }
        });

        btn_cancelarArea.setText("Cancelar");
        btn_cancelarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarAreaActionPerformed(evt);
            }
        });

        btn_editarArea.setText("Editar");
        btn_editarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarAreaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_areaLayout = new javax.swing.GroupLayout(panel_area);
        panel_area.setLayout(panel_areaLayout);
        panel_areaLayout.setHorizontalGroup(
            panel_areaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_areaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panel_areaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_areaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_areaLayout.createSequentialGroup()
                            .addComponent(labelD_nombre)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtF_nombreArea)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panel_areaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(panel_areaLayout.createSequentialGroup()
                                    .addComponent(btn_buscarArea, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btn_cancelarArea, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panel_areaLayout.createSequentialGroup()
                                    .addComponent(btn_guardarArea, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(btn_editarArea, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 388, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_areaLayout.createSequentialGroup()
                        .addComponent(labelT_area, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(separator2, javax.swing.GroupLayout.PREFERRED_SIZE, 367, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        panel_areaLayout.setVerticalGroup(
            panel_areaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_areaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_areaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelT_area)
                    .addComponent(separator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(panel_areaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panel_areaLayout.createSequentialGroup()
                        .addGroup(panel_areaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtF_nombreArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelD_nombre)
                            .addComponent(btn_guardarArea)
                            .addComponent(btn_editarArea, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_buscarArea))
                    .addComponent(btn_cancelarArea))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel_buscarDocente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panel_asignarTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panel_area, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(panel_tabladocente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel_area, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel_asignarTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel_buscarDocente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel_tabladocente, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        /*if (txtF_busqueda.getText().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "¡Debes introducir un nombre!");
        } else {
            String valor = txtF_busqueda.getText();
            String parametro = "nombre";
            mostrarDatosTitulo(parametro, valor);
    }*/
        
        
    }//GEN-LAST:event_btn_buscarActionPerformed

    private void menuIt_editarTituloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_editarTituloActionPerformed
        int filaSeleccionada = table_AreaAcreditacion.getSelectedRow();

        idEditarAcreditacionDocente = Integer.parseInt(table_AreaAcreditacion.getModel().getValueAt(filaSeleccionada, 1).toString());
        
        txtF_titulo.setText(table_AreaAcreditacion.getValueAt(filaSeleccionada, 1).toString().trim());
    /*int idD = Integer.parseInt(table_AreaAcreditacion.getModel().getValueAt(filaSeleccionada, 2).toString());
        int idA = Integer.parseInt(table_AreaAcreditacion.getModel().getValueAt(filaSeleccionada, 3).toString());
        
        for (int i = 0; i < comboB_docente.getItemCount(); i++) {
            if(comboB_docente.getItemAt(i).getId() == idD){
                comboB_docente.setSelectedIndex(i);
            }
        }
        
        for (int i = 0; i < comboB_area.getItemCount(); i++) {
            if(comboB_area.getItemAt(i).getId() == idA){
                comboB_area.setSelectedIndex(i);
            }
        }*/

        btn_editarTitulo.setEnabled(true);
        btn_guardarTitulo.setEnabled(false);
    }//GEN-LAST:event_menuIt_editarTituloActionPerformed

    private void menuIt_borrarTituloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_borrarTituloActionPerformed
        int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "¡Alerta!", JOptionPane.YES_NO_OPTION);
        if (resp == 0) {
            int filaSeleccionada = table_AreaAcreditacion.getSelectedRow();
            idEditarAcreditacionDocente = Integer.parseInt(table_AreaAcreditacion.getModel().getValueAt(filaSeleccionada, 1).toString());
            baseDatosAcreditacionDocente.delete(idEditarAcreditacionDocente);
            JOptionPane.showMessageDialog(rootPane, "¡Borrado con Éxito!");
            mostrarDatosTitulo("", "");
            idEditarAcreditacionDocente = 0;
        }
    }//GEN-LAST:event_menuIt_borrarTituloActionPerformed

    private void txtF_tituloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtF_tituloActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtF_tituloActionPerformed

    private void comboB_busquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboB_busquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboB_busquedaActionPerformed

    private void btn_guardarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarAreaActionPerformed
        if (txtF_nombreArea.getText().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "¡Debes introducir un nombre!");
        } else {
            baseDatosAreaAcreditaciones.guardar(txtF_nombreArea.getText());
            JOptionPane.showMessageDialog(rootPane, "¡Guardado con Éxito!");
            //vaciarArea();
            cargarDatos();
            mostrarDatosArea("", "");
        }
    }//GEN-LAST:event_btn_guardarAreaActionPerformed

    private void btn_cancelarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarAreaActionPerformed
        if (btn_guardarArea.isEnabled()) {
            vaciarArea();
            mostrarDatosArea("", "");
        } else {
            //btn_editarArea.setEnabled(false);
            btn_guardarArea.setEnabled(true);
            btn_buscarArea.setEnabled(true);
            vaciarArea();
            mostrarDatosArea("", "");
            idEditarAreaAcreditacion = 0;
        }
    }//GEN-LAST:event_btn_cancelarAreaActionPerformed

    private void btn_buscarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarAreaActionPerformed
    /*    if (txtF_nombreArea.getText().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "¡Debes introducir un nombre!");
        } else {
            String valor = txtF_nombreArea.getText();
            String parametro = "nombre";
            mostrarDatosArea(parametro, valor);
    } */
    }//GEN-LAST:event_btn_buscarAreaActionPerformed

    private void btn_guardarTituloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarTituloActionPerformed
        if(txtF_titulo.getText().equals("")){
           JOptionPane.showMessageDialog(rootPane, "¡No puedes guardar un valor vacio!");
        }else{
            Docente doce  = new Docente();
            doce = (Docente)comboB_docente.getSelectedItem();
            Especialidad esp = new Especialidad();
            esp = (Especialidad)comboB_area.getSelectedItem();

           baseDatosAcreditacionDocente.guardar(doce.getId(),txtF_titulo.getText(), esp.getId());
            JOptionPane.showMessageDialog(rootPane, "¡Guardado con Éxito!");        
            vaciarTitulo();
            cargarDatos();
        }
    }//GEN-LAST:event_btn_guardarTituloActionPerformed

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        if (btn_guardarTitulo.isEnabled()) {
            vaciarTitulo();
            mostrarDatosTitulo("", "");
        } else {
            btn_editarTitulo.setEnabled(false);
            btn_guardarTitulo.setEnabled(true);
            vaciarTitulo();
            mostrarDatosTitulo("", "");
            idEditarAcreditacionDocente = 0;
        }
    }//GEN-LAST:event_btn_cancelarActionPerformed

    private void txtF_nombreAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtF_nombreAreaActionPerformed
        //
    }//GEN-LAST:event_txtF_nombreAreaActionPerformed

    private void comboB_docenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboB_docenteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboB_docenteActionPerformed

    private void txtF_busquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtF_busquedaActionPerformed
       //
    }//GEN-LAST:event_txtF_busquedaActionPerformed

    private void btn_editarTituloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarTituloActionPerformed
        if(txtF_titulo.getText().equals("")){
          JOptionPane.showMessageDialog(rootPane, "¡No puedes guardar un valor vacio!");
        }else{
            Docente doce  = new Docente();
            doce = (Docente)comboB_docente.getSelectedItem();
            Especialidad esp = new Especialidad();
            esp = (Especialidad)comboB_area.getSelectedItem();

           baseDatosAcreditacionDocente.actualizar(idEditarAcreditacionDocente, doce.getId(), txtF_titulo.getText(), esp.getId());
            JOptionPane.showMessageDialog(rootPane, "¡Editado con Éxito!");        
            vaciarTitulo();
            cargarDatos();
            mostrarDatosTitulo("","");
        }
    }//GEN-LAST:event_btn_editarTituloActionPerformed

    private void btn_editarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarAreaActionPerformed
        if(txtF_titulo.getText().equals("")){
          JOptionPane.showMessageDialog(rootPane, "¡No puedes guardar un valor vacio!");
        }else{
           baseDatosAreaAcreditaciones.actualizar(idEditarAreaAcreditacion, txtF_nombreArea.getText());
            JOptionPane.showMessageDialog(rootPane, "¡Editado con Éxito!");        
            vaciarTitulo();
            cargarDatos();
            mostrarDatosTitulo("",""); 
        }
    }//GEN-LAST:event_btn_editarAreaActionPerformed

    private void menuIt_editarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_editarAreaActionPerformed
        int filaSeleccionada = table_area.getSelectedRow();

        idEditarAreaAcreditacion = Integer.parseInt(table_area.getModel().getValueAt(filaSeleccionada, 1).toString());
        txtF_titulo.setText(table_area.getValueAt(filaSeleccionada, 1).toString().trim());

        btn_editarArea.setEnabled(true);
        btn_guardarArea.setEnabled(false);
        //btn_buscarArea.setEnabled(false);
  
    }//GEN-LAST:event_menuIt_editarAreaActionPerformed

    private void menuIt_borrarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_borrarAreaActionPerformed
    int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "¡Alerta!", JOptionPane.YES_NO_OPTION);

    if (resp == 0) {
        int filaSeleccionada = table_area.getSelectedRow();
          idEditarAreaAcreditacion = Integer.parseInt(table_area.getModel().getValueAt(filaSeleccionada, 1).toString());
          baseDatosAreaAcreditaciones.delete(idEditarAreaAcreditacion);
          JOptionPane.showMessageDialog(rootPane, "¡Borrado con Éxito!");
          mostrarDatosArea("", "");
       idEditarAreaAcreditacion = 0;
        }
    }//GEN-LAST:event_menuIt_borrarAreaActionPerformed

    // Own functions ***********************************************************
    private void cargarDatos() {
        mostrarDatosTitulo("", "");
        mostrarDatosArea("", "");
        mostrarListaTitulos();
        mostrarListaAreas();
    }

    private void mostrarDatosTitulo(String parametro, String valor) {
        btn_editarTitulo.setEnabled(false);                           //esconder el boton editar
        btn_buscar.setEnabled(false);
        DefaultTableModel modelo = new DefaultTableModel();     //establecer modelo de la tabla
        modelo.addColumn("Contador");
        modelo.addColumn("Id");
        modelo.addColumn("Titulo");
        modelo.addColumn("Docente");
        modelo.addColumn("Area");
        String[] datos = new String[5];                         //cargar los datos a la tabla

        try {
            ResultSet rs = baseDatosAcreditacionDocente.buscarPor(parametro, valor);
            int i = 1;

            while (rs.next()) {

                datos[0] = String.valueOf(i);                       //Contador
                datos[1] = rs.getString(1);                         //Id
                datos[2] = rs.getString(2);                         //Docente
                datos[3] = rs.getString(3) + " " + rs.getString(4);  //Titulo
                datos[4] = rs.getString(5);                         //Area
                modelo.addRow(datos);
                i++;
            }

            table_AreaAcreditacion.setModel(modelo);
            TableColumnModel tcm = table_AreaAcreditacion.getColumnModel();
            tcm.removeColumn(tcm.getColumn(1)); //ocultar una columna
        } catch (SQLException ex) {
            System.out.println("Algo falló" + ex);
        }
    }

    private void mostrarDatosArea(String parametro, String valor) {
        btn_editarArea.setEnabled(false);                           //esconder el boton editar
        btn_buscarArea.setEnabled(false);
        DefaultTableModel modelo = new DefaultTableModel();     //establecer modelo de la tabla
        modelo.addColumn("Contador");
        modelo.addColumn("Nombre");
        String[] datos = new String[2];                         //cargar los datos a la tabla

        try {

            ResultSet rs = baseDatosAreaAcreditaciones.buscarPor(parametro, valor);
            int i = 1;

            while (rs.next()) {

                datos[0] = String.valueOf(i);                   //Contador
                datos[1] = rs.getString(1);                     //Nombre
                modelo.addRow(datos);
                i++;
            }

            table_area.setModel(modelo);

        } catch (SQLException ex) {
            System.out.println("Algo falló" + ex);
        }
    }

    private void mostrarListaTitulos() {
        comboB_docente.removeAllItems();
        listaDocentes = baseDatosDocentes.getListaDocentes();
        Iterator iterador = listaDocentes.iterator();
        while (iterador.hasNext()) {
            Docente docente = (Docente) iterador.next();
            comboB_docente.addItem(docente);
        }
        AutoCompleteDecorator.decorate(comboB_docente);
    } 

    private void mostrarListaAreas() {
        comboB_area.removeAllItems();
        listaAreas = baseDatosAreaAcreditaciones.getListaAreas();
        Iterator iterador = listaAreas.iterator();
        while (iterador.hasNext()) {
            Especialidad espec = (Especialidad) iterador.next();
            comboB_area.addItem(espec);
        }
        AutoCompleteDecorator.decorate(comboB_area);
    }

    private void vaciarTitulo() {
        comboB_docente.setSelectedIndex(0);
        txtF_titulo.setText("");
        comboB_area.setSelectedIndex(0);
    }

    private void vaciarArea() {
        txtF_nombreArea.setText("");
    }

    //MAIN     
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ABM_CV_Docentes().setVisible(true);
            }
        });
    }
    //Variables Propias
    private ConexionDocentes baseDatosDocentes = new ConexionDocentes();
    private ConexionAcreditacionesDocentes baseDatosAcreditacionDocente = new ConexionAcreditacionesDocentes();
    private ConexionAreaAcreditaciones baseDatosAreaAcreditaciones = new ConexionAreaAcreditaciones();
    private ArrayList<Docente> listaDocentes;
    private ArrayList<Area> listaAreas;
    private Integer idEditarAcreditacionDocente = 0;
    private Integer idEditarAreaAcreditacion = 0;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_buscarArea;
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_cancelarArea;
    private javax.swing.JButton btn_editarArea;
    private javax.swing.JButton btn_editarTitulo;
    private javax.swing.JButton btn_guardarArea;
    private javax.swing.JButton btn_guardarTitulo;
    private javax.swing.JComboBox<Especialidad> comboB_area;
    private javax.swing.JComboBox<String> comboB_busqueda;
    private javax.swing.JComboBox<Docente> comboB_docente;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel labelD_area;
    private javax.swing.JLabel labelD_docente;
    private javax.swing.JLabel labelD_nombre;
    private javax.swing.JLabel labelD_titulo;
    private javax.swing.JLabel labelT_area;
    private javax.swing.JLabel labelT_asignarTitulo;
    private javax.swing.JLabel labelT_buscar;
    private javax.swing.JMenuItem menuIt_borrarArea;
    private javax.swing.JMenuItem menuIt_borrarTitulo;
    private javax.swing.JMenuItem menuIt_editarArea;
    private javax.swing.JMenuItem menuIt_editarTitulo;
    private javax.swing.JPopupMenu opcionesArea_popupMenu;
    private javax.swing.JPopupMenu opciones_popupMenu;
    private javax.swing.JPanel panel_area;
    private javax.swing.JPanel panel_asignarTitulo;
    private javax.swing.JPanel panel_buscarDocente;
    private javax.swing.JPanel panel_tabladocente;
    private javax.swing.JSeparator separator1;
    private javax.swing.JSeparator separator2;
    private javax.swing.JSeparator separator3;
    private javax.swing.JTable table_AreaAcreditacion;
    private javax.swing.JTable table_area;
    private javax.swing.JTextField txtF_busqueda;
    private javax.swing.JTextField txtF_nombreArea;
    private javax.swing.JTextField txtF_titulo;
    // End of variables declaration//GEN-END:variables

}
