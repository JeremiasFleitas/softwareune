/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiones;

import clasesIniciales.Acreditacion;
import clasesIniciales.Especialidad;
import clasesIniciales.Docente;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jeremías
 */
public class ConexionAcreditacionesDocentes {
    public Connection baseDatos = Conexion.conexion();
    //Nombre de las columnas
    String id="id_titulo_docente";
    String titulo="titulo_docente";
    String idDocente="id_docente";
    String idAcre="id_area_acreditacion";
    String tabla = "acreditaciones_docentes";
    Conexion cc= new Conexion ();
    

      public ResultSet buscarPor(String parametro, String valor){
        String sql="";
        sql = "SELECT id_titulo_docente,acreditaciones_docentes.titulo_docente,docentes.nombre_docente,docentes.apellido_docente,area_acreditaciones.nombre_area "
                +" FROM acreditaciones_docentes,docentes,area_acreditaciones "
                +" WHERE acreditaciones_docentes.id_docente = docentes.id_docente "
                +" AND acreditaciones_docentes.id_area_acreditacion = area_acreditaciones.id_area_acreditacion";
                //+" AND titulo_docente = '" + valor + "'"
        return procesarSQL(sql);
    } 

    public void guardar( int idD, String tituloDoc,int idA){
        String sql = "INSERT INTO " + tabla + "(" + idDocente + "," + titulo + "," + idAcre + ") "
                     + "VALUES (" + idD + ",'" + tituloDoc + "'," + idA + ")";
         System.out.println(sql);
        ejecutarSQL(sql);
    }
    
        
   public void actualizar(int idAD, int idD, String tituloDoc,  int idA){
        String sql = "UPDATE " + tabla + " SET " +idDocente+ " = " + idD + ", " + titulo + " = '" + tituloDoc + "', " +idAcre+ " = " +idA+" WHERE "+this.id+"="+idAD;
        ejecutarSQL(sql);
    }
    
    public void delete(int idA){
        String sql = "DELETE FROM " + tabla + " WHERE " + this.id + "=" + idA;
        ejecutarSQL(sql);
    }
    
     private ResultSet procesarSQL(String sql){
        try {
            Statement st = (Statement) baseDatos.createStatement();
            ResultSet rs = st.executeQuery(sql);
            return rs;
            
        } catch (SQLException ex) {
            Logger.getLogger(ConexionAcreditacionesDocentes.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
     private void ejecutarSQL(String sql){
        try{
            PreparedStatement pst = (PreparedStatement) baseDatos.prepareStatement(sql);
            pst.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     } 
}
