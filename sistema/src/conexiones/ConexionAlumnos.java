/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiones;




import clasesIniciales.Alumno;
import conexiones.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando
 */
public class ConexionAlumnos {
   
    public Connection baseDatos = Conexion.conexion();
    //Nombre de las columnas
    String id="id_alumno";
    String nombre="nombre_alumno";
    String apellido="apellido_alumno";
    String numDoc="documento_alumno";
    String tipoDoc="tipo_doc_alumno";
    String contacto="contacto_alumno";
    String email="email_alumno";
    
    public ResultSet buscarPor(String parametro, String valor){
        String sql="";
        if (parametro.toLowerCase().equals("nombre")){
            sql="SELECT * FROM alumnos WHERE "+nombre+" LIKE '%"+valor+"%'"+" AND NOT id_alumno = 0";
        }else if (parametro.toLowerCase().equals("apellido")){
            sql="SELECT * FROM alumnos WHERE "+apellido+" LIKE '%"+valor+"%'"+" AND NOT id_alumno = 0";
        }else if (parametro.toLowerCase().equals("documento")){
            sql="SELECT * FROM alumnos WHERE "+numDoc+" LIKE '%"+valor+"%'"+" AND NOT id_alumno = 0";
        }else if (parametro.toLowerCase().equals("contacto")){
            sql="SELECT * FROM alumnos WHERE "+contacto+" LIKE '%"+valor+"%'"+" AND NOT id_alumno = 0";
        }else if (parametro.toLowerCase().equals("email")){
            sql="SELECT * FROM alumnos WHERE "+email+" LIKE '%"+valor+"%'"+" AND NOT id_alumno = 0";
        }else if (parametro.toLowerCase().equals("id")){
             sql = "SELECT * FROM alumnos WHERE " + id + "='" + valor + "'"+" AND NOT id_alumno = 0";
        }else{
            sql="SELECT * FROM alumnos"+" WHERE NOT id_alumno = 0";
        }
        return procesarSQL(sql);
    }
    
    public void guardar(String nom, String apell, String doc, String modeloDoc, String contact, String correo){
        String sql = "INSERT INTO alumnos(" + nombre + "," + apellido + "," + numDoc + "," + tipoDoc + "," + contacto + "," + email + ") "
                     + "VALUES ('" + nom + "','" + apell + "','" + doc + "','" + modeloDoc + "','" + contact + "','" + correo + "')"; 
        System.out.println(sql);
        ejecutarSQL(sql);
    }
    
    public void actualizar(int id, String nom, String apell, String doc, String modeloDoc, String contact, String correo){
        String sql = "UPDATE alumnos SET " + nombre + "='" + nom + "', " + apellido + "='" + apell + "', " + numDoc + "='" + doc + "', " 
                + tipoDoc + "='" + modeloDoc + "', " + contacto + "='" + contact + "', " + email + "='" + correo + "' WHERE "+this.id+"="+id;
        ejecutarSQL(sql);
    }
    
    public void delete(int id){
        String sql = "DELETE FROM alumnos WHERE "+this.id+"='"+id+"'";
        ejecutarSQL(sql);
    }
    
     private ResultSet procesarSQL(String sql){
        try {
            Statement st = (Statement) baseDatos.createStatement();
            ResultSet rs = st.executeQuery(sql);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionAlumnos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
     private void ejecutarSQL(String sql){
        try{
            PreparedStatement pst = (PreparedStatement) baseDatos.prepareStatement(sql);
            pst.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     }   

    public ArrayList getListaAlumnos() {
    ArrayList listaAlumnos = new ArrayList();
        Alumno alumno = null;
        Statement consulta;
        ResultSet resultado;
        try {
            consulta = (Statement) baseDatos.createStatement();
            resultado = consulta.executeQuery("SELECT * FROM alumnos");
            while(resultado.next()){
                alumno = new Alumno();
                alumno.setId_alumno(resultado.getInt("id_alumno"));
                alumno.setNombre_alumno(resultado.getString("nombre_alumno"));
                listaAlumnos.add(alumno);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionCarreras.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaAlumnos;
    }
   
}
