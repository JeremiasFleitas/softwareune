/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiones;




import clasesIniciales.Carrera;
import conexiones.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando
 */
  public class ConexionCarreras {
   
    public Connection baseDatos = Conexion.conexion();
    //Nombre de las columnas
    String id="id_carrera";
    String nombre="nombre_carrera";
    String descripcion="descripcion_carrera";
    String tabla = "carreras";
    
    public ResultSet buscarPor(String parametro, String valor){
        String sql="";
        if (parametro.toLowerCase().equals("nombre")){
            sql="SELECT * FROM " + tabla + " WHERE "+nombre+" LIKE '%"+valor+"%'";
        }else if (parametro.toLowerCase().equals("descripcion")){
            sql="SELECT * FROM " + tabla + " WHERE "+descripcion+" LIKE '%"+valor+"%'";
        }else if (parametro.toLowerCase().equals("id")){
             sql = "SELECT * FROM " + tabla + " WHERE " + id + "='" + valor + "'";
        }else{
            sql="SELECT * FROM carreras";
        }
        return procesarSQL(sql);
    }
    
    public void guardar(String nom, String descrip){
        String sql = "INSERT INTO carreras(" + nombre + "," + descripcion + ") "
                     + "VALUES ('" + nom + "','" + descrip + "')"; 
        System.out.println(sql);
        ejecutarSQL(sql);
    }
    
    public void actualizar(int id, String nom, String descrip){
        String sql = "UPDATE carreras SET " + nombre + "='" + nom + "', " + descripcion + "='" + descrip + "' WHERE "+this.id+"="+id;
        ejecutarSQL(sql);
    }
    
    public void delete(int id){
        String sql = "DELETE FROM carreras WHERE "+this.id+"='"+id+"'";
        ejecutarSQL(sql);
    }
    
     private ResultSet procesarSQL(String sql){
        try {
            Statement st = (Statement) baseDatos.createStatement();
            ResultSet rs = st.executeQuery(sql);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionCarreras.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
     private void ejecutarSQL(String sql){
        try{
            PreparedStatement pst = (PreparedStatement) baseDatos.prepareStatement(sql);
            pst.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     } 
     
     
     public ArrayList getListaCarreras(){
        ArrayList listaCarreras = new ArrayList();
        Carrera carrera = null;
        Statement consulta;
        ResultSet resultado;
        try {
            consulta = (Statement) baseDatos.createStatement();
            resultado = consulta.executeQuery("SELECT * FROM carreras");
            while(resultado.next()){
                carrera = new Carrera();
                carrera.setId(resultado.getInt("id_carrera"));
                carrera.setNombre(resultado.getString("nombre_carrera"));
                listaCarreras.add(carrera);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionCarreras.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaCarreras;
    }
      
   
}
