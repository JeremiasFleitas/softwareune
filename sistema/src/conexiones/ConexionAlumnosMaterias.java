/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conexiones;

import clasesIniciales.Alumno;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nando
 */
public class ConexionAlumnosMaterias {
    public Connection baseDatos = Conexion.conexion();
    //Nombre de las columnas
    String id="id_alumno_materia_periodo";
    String estado="estado_alumno_materia";
    String alumno="id_alumno";
    String materiaPer = "id_materia_periodo";
    String tabla = "alumnos_materias_periodo";
    
    public ResultSet buscarPor(String idAlumno, String idCarrera){
        String sql="SELECT id_alumno_materia_periodo, c.id_carrera, c.nombre_carrera, mp.id_materia_periodo,m.id_materia, m.nombre_materia, a.id_alumno, estado_alumno_materia FROM alumnos_materias_periodo amp, materias_periodo mp, carreras c, materias m, alumnos a WHERE amp.id_materia_periodo = mp.id_materia_periodo AND mp.id_materia = m.id_materia AND mp.id_carrera = c.id_carrera AND amp.id_alumno = a.id_alumno";
        sql = sql.concat(" AND a.id_alumno = "+idAlumno + " AND c.id_carrera = "+idCarrera);
        
        return procesarSQL(sql);
    }
    
    /**public ResultSet buscarPor2(String idCarrera, String idAlumno){
        String sql="SELECT `id_alumno_carrera`,ac.id_carrera,c.nombre_carrera,`estado_alumno_carrera`,`id_alumno` FROM alumnos_carreras ac, carreras c WHERE ac.id_carrera = c.id_carrera";
        
        sql.concat(" AND ac.id_carrera = " + idCarrera + " AND id_alumno = " + idAlumno);
        
        return procesarSQL(sql);
    }**/
    
    public void guardar(String estadoA, String idA, String idMP){
        String sql = "INSERT INTO alumnos_materias_periodo(" + estado + "," + alumno + "," + materiaPer +") "
                     + "VALUES ('" + estadoA + "'," + idA + "," + idMP +")"; 
        
        ejecutarSQL(sql);
    }
    
    public void actualizarEstado(String idAM, String esta){
        String sql = "UPDATE alumnos_materias_periodo SET " + estado + "='" + esta + "' WHERE "+this.id+"="+idAM;
        ejecutarSQL(sql);
    }
    
    public void delete(String id){
        String sql = "DELETE FROM alumnos_materias_periodo WHERE "+this.id+"='"+id+"'";
        ejecutarSQL(sql);
    }
    
    public void deleteAll(String idMateriaPeriodo){
        String sql = "DELETE FROM alumnos_materias_periodo WHERE "+materiaPer+"="+idMateriaPeriodo;
        ejecutarSQL(sql);
    }
    
     private ResultSet procesarSQL(String sql){
        try {
            Statement st = (Statement) baseDatos.createStatement();
            ResultSet rs = st.executeQuery(sql);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionCarreras.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
     private void ejecutarSQL(String sql){
        try{
            PreparedStatement pst = (PreparedStatement) baseDatos.prepareStatement(sql);
            pst.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     } 
     
     
     public ArrayList getListaAlumnos(int idMateriaPeriodo){
        ArrayList listaAlumnos = new ArrayList();
        Alumno al = null;
        Statement consulta;
        ResultSet resultado;
        try {
            consulta = (Statement) baseDatos.createStatement();
            resultado = consulta.executeQuery("SELECT id_alumno_materia_periodo, amp.id_materia_periodo, a.id_alumno, a.nombre_alumno, a.apellido_alumno FROM alumnos_materias_periodo amp, alumnos a WHERE amp.id_alumno = a.id_alumno AND amp.id_materia_periodo="+idMateriaPeriodo);
            while(resultado.next()){
                al = new Alumno();
                al.setId_alumno(resultado.getInt("id_alumno"));
                al.setNombre_alumno(resultado.getString("nombre_alumno")+" "+resultado.getString("apellido_alumno"));
                listaAlumnos.add(al);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionCarreras.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaAlumnos;
    }

    public ResultSet buscarPor(int idAlumno, int idCarrera) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
}
