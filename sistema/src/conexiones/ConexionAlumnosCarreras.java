/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conexiones;

import clasesIniciales.Carrera;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nando
 */
public class ConexionAlumnosCarreras {
    public Connection baseDatos = Conexion.conexion();
    //Nombre de las columnas
    String id="id_alumno_carrera";
    String estado="estado_alumno_carrera";
    String alumno="id_alumno";
    String carrera = "id_carrera";
    String tabla = "alumnos_carreras";
    
    public ResultSet buscarPor(String parametro, String valor){
        String sql="SELECT `id_alumno_carrera`,ac.id_carrera,c.nombre_carrera,`estado_alumno_carrera`,`id_alumno` FROM alumnos_carreras ac, carreras c WHERE ac.id_carrera = c.id_carrera";
        
        if (parametro.toLowerCase().equals("estado")){
            sql = sql.concat(" AND estado_alumno_carrera LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("idalumno")){
            sql = sql.concat(" AND id_alumno = "+valor);
            
        }else if (parametro.toLowerCase().equals("idcarrera")){
            sql = sql.concat(" AND ac.id_carrera = "+valor);
        }
        
        return procesarSQL(sql);
    }
    
    public ResultSet buscarPor2(String idCarrera, String idAlumno){
        String sql="SELECT `id_alumno_carrera`,ac.id_carrera,c.nombre_carrera,`estado_alumno_carrera`,`id_alumno` FROM alumnos_carreras ac, carreras c WHERE ac.id_carrera = c.id_carrera";
        
        sql = sql.concat(" AND ac.id_carrera = " + idCarrera + " AND id_alumno = " + idAlumno);
        
        return procesarSQL(sql);
    }
    
    public void guardar(String estadoC, String idA, String idC){
        String sql = "INSERT INTO alumnos_carreras(" + estado + "," + alumno + "," + carrera +") "
                     + "VALUES ('" + estadoC + "'," + idA + "," + idC +")"; 
        
        ejecutarSQL(sql);
    }
    
    public void actualizarEstado(String idAC, String esta){
        String sql = "UPDATE alumnos_carreras SET " + estado + "='" + esta + "' WHERE "+this.id+"="+idAC;
        ejecutarSQL(sql);
    }
    
    public void delete(String id){
        String sql = "DELETE FROM alumnos_carreras WHERE "+this.id+"='"+id+"'";
        ejecutarSQL(sql);
    }
   
    
     private ResultSet procesarSQL(String sql){
        try {
            Statement st = (Statement) baseDatos.createStatement();
            ResultSet rs = st.executeQuery(sql);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionCarreras.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
     private void ejecutarSQL(String sql){
        try{
            PreparedStatement pst = (PreparedStatement) baseDatos.prepareStatement(sql);
            pst.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     } 
     
     
     public ArrayList getListaCarreras(){
        ArrayList listaCarreras = new ArrayList();
        Carrera carrera = null;
        Statement consulta;
        ResultSet resultado;
        try {
            consulta = (Statement) baseDatos.createStatement();
            resultado = consulta.executeQuery("SELECT * FROM carreras");
            while(resultado.next()){
                carrera = new Carrera();
                carrera.setId(resultado.getInt("id_carrera"));
                carrera.setNombre(resultado.getString("nombre_carrera"));
                listaCarreras.add(carrera);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionCarreras.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaCarreras;
    }
}
