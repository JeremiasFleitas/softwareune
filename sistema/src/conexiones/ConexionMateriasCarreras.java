/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiones;




import clasesIniciales.Materia;
import conexiones.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando
 */
public class ConexionMateriasCarreras {
   
    public Connection baseDatos = Conexion.conexion();
    //Nombre de las columnas
    String id="id_materia_periodo";
    String inicio="fecha_inicio";
    String fin="fecha_fin";
    String hs="hs_semana";
    String estado="estado_materia_periodo";
    String idMateria = "id_materia";
    String idDocente = "id_docente";
    String idCarrera = "id_carrera";
    String tabla = "materias_periodo";
    
    public ResultSet buscarPor(String parametro, String valor){
        String sql="";
        
        sql = "SELECT id_materia_periodo, mp.id_materia, m.nombre_materia, mp.id_docente, d.nombre_docente, d.apellido_docente, mp.id_carrera, c.nombre_carrera, hs_semana, fecha_inicio, fecha_fin, estado_materia_periodo FROM materias_periodo mp, carreras c, docentes d, materias m WHERE mp.id_materia = m.id_materia AND mp.id_docente = d.id_docente AND mp.id_carrera = c.id_carrera";
           
        if (parametro.toLowerCase().equals("materia")){
            sql = sql.concat(" AND m.nombre_materia LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("docente")){
            sql = sql.concat(" AND d.nombre_docente LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("carrera")){
            sql = sql.concat(" AND c.nombre_carrera LIKE '%"+valor+"%'"); 
        }else if (parametro.toLowerCase().equals("idcarrera")){
            sql = sql.concat(" AND mp.id_carrera = "+valor); 
        }else if (parametro.toLowerCase().equals("estado")){
            sql = sql.concat(" AND estado_materia_periodo LIKE '%"+valor+"%'"); 
        }
        
        return procesarSQL(sql);
    }
    
    public ResultSet buscarPor2(String parametro, String valor, String parametro2, String valor2){
        
        String sql="";
        
        sql = "SELECT id_materia_periodo, mp.id_materia, m.nombre_materia, mp.id_docente, d.nombre_docente, d.apellido_docente, mp.id_carrera, c.nombre_carrera, hs_semana, fecha_inicio, fecha_fin, estado_materia_periodo FROM materias_periodo mp, carreras c, docentes d, materias m WHERE mp.id_materia = m.id_materia AND mp.id_docente = d.id_docente AND mp.id_carrera = c.id_carrera";
           
        if (parametro.toLowerCase().equals("materia")){
            sql = sql.concat(" AND m.nombre_materia LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("docente")){
            sql = sql.concat(" AND d.nombre_docente LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("carrera")){
            sql = sql.concat(" AND c.nombre_carrera LIKE '%"+valor+"%'"); 
        }else if (parametro.toLowerCase().equals("idcarrera")){
            sql = sql.concat(" AND mp.id_carrera = "+valor); 
        }else if (parametro.toLowerCase().equals("estado")){
            sql = sql.concat(" AND estado_materia_periodo = '"+valor+"'"); 
        }
        
        if (parametro2.toLowerCase().equals("materia")){
            sql = sql.concat(" AND m.nombre_materia LIKE '%"+valor2+"%'");
        }else if (parametro2.toLowerCase().equals("docente")){
            sql = sql.concat(" AND d.nombre_docente LIKE '%"+valor2+"%'");
        }else if (parametro2.toLowerCase().equals("carrera")){
            sql = sql.concat(" AND c.nombre_carrera LIKE '%"+valor2+"%'"); 
        }else if (parametro2.toLowerCase().equals("idcarrera")){
            sql = sql.concat(" AND mp.id_carrera = "+valor2); 
        }else if (parametro2.toLowerCase().equals("estado") && !(valor2.equalsIgnoreCase("todos"))){
            sql = sql.concat(" AND estado_materia_periodo = '"+valor2+"'"); 
        }
        
        return procesarSQL(sql);
    }
    
    public void guardar(String fechaInicio, String fechaFin, String hsSemana, int idC, int idD, int idM){
        String sql = "INSERT INTO " + tabla + "(" + inicio + "," + fin +  "," + hs +  "," + idMateria + "," + idDocente + "," + idCarrera + "," + estado +") "
                     + "VALUES ('" + fechaInicio + "','" + fechaFin + "','" + hsSemana + "','" + idM + "','" + idD + "','" + idC + "','" + "Activo" + "')"; 
        ejecutarSQL(sql);
    }
   
    
    public void actualizar(int idMC, String fechaInicio, String fechaFin, String hsSemana, int idC, int idD, int idM){
        String sql = "UPDATE " + tabla + " SET " + inicio + "='" + fechaInicio + "', " + fin + "='" + fechaFin + "', " + hs + "='" + hsSemana + "', " + idMateria + "='" +idM+ "', " +idDocente+ "='" + idD + "', " +idCarrera+ "='" +idC+"' WHERE "+this.id+"="+idMC;
        ejecutarSQL(sql);
    }
    
    public void actualizarEstado(String idMC, String estad){
        String sql = "UPDATE " + tabla + " SET " + estado + "='" + estad + "' WHERE "+this.id+"="+idMC;
        ejecutarSQL(sql);
    }
    
    public void delete(int id){
        String sql = "DELETE FROM " + tabla + " WHERE " + this.id + "=" + id;
        ejecutarSQL(sql);
    }
    
     private ResultSet procesarSQL(String sql){
        try {
            Statement st = (Statement) baseDatos.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionMateriasCarreras.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
     private void ejecutarSQL(String sql){
        try{
            PreparedStatement pst = (PreparedStatement) baseDatos.prepareStatement(sql);
            pst.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     } 
     
     
     public ArrayList getListaMateriasPeriodo(int idCarrera){
        ArrayList listaMateriasPeriodo = new ArrayList();
        Materia mater = null;
        Statement consulta;
        ResultSet resultado;
        try {
            consulta = (Statement) baseDatos.createStatement();
            resultado = consulta.executeQuery("SELECT id_materia_periodo, m.nombre_materia FROM materias_periodo mp, materias m, carreras c WHERE mp.id_materia = m.id_materia AND mp.id_carrera = c.id_carrera AND mp.id_carrera = "+idCarrera);
            while(resultado.next()){
                mater = new Materia();
                mater.setId(resultado.getInt("id_materia_periodo"));
                mater.setNombre(resultado.getString("nombre_materia"));
                listaMateriasPeriodo.add(mater);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionCarreras.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaMateriasPeriodo;
    }
      
   
}
