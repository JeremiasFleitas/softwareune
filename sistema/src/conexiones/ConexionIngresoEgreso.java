/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conexiones;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nando
 */
public class ConexionIngresoEgreso {
    public Connection baseDatos = Conexion.conexion();
    //Nombre de las columnas
    String id="id_ingreso_egreso";
    String tipo="tipo_ingreso_egreso";
    String numFac="factura_ingreso_egreso";
    String monto="monto_ingreso_egreso";
    String concepto="concepto_ingreso_egreso";
    String idAlumno="id_alumno";
    String idDocente="id_docente";
    String tabla = "ingresos_egresos";
    
    public ResultSet buscarPor(String parametro, String valor){
        String sql="";
        sql = "SELECT id_ingreso_egreso, tipo_ingreso_egreso, factura_ingreso_egreso, monto_ingreso_egreso, concepto_ingreso_egreso, "
                + "d.id_docente, d.documento_docente, d.nombre_docente, "
                + "a.id_alumno, a.documento_alumno, a.nombre_alumno, created_at "
                + "FROM ingresos_egresos ie, alumnos a, docentes d "
                + "WHERE ie.id_alumno = a.id_alumno AND ie.id_docente=d.id_docente";
        
        if (parametro.toLowerCase().equals("tipo")){
            sql = sql.concat(" AND tipo_ingreso_egreso LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("factura")){
            sql = sql.concat(" AND factura_ingreso_egreso LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("monto")){
            sql = sql.concat(" AND monto_ingreso_egreso LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("concepto")){
            sql = sql.concat(" AND concepto_ingreso_egreso LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("docente")){
            sql = sql.concat(" AND d.nombre_docente LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("cin docente")){
            sql = sql.concat(" AND d.documento_docente LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("alumno")){
            sql = sql.concat(" AND a.nombre_alumno LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("cin alumno")){
            sql = sql.concat(" AND a.documento_alumno LIKE '%"+valor+"%'");
        }
        return procesarSQL(sql);
    }
    
    public void guardar(String tipoF, String numF, String montoF, String concept, int idAlu, int idDoc){
        String sql = "INSERT INTO " + tabla + "(" + tipo + "," + numFac + "," + monto + "," + concepto + "," + idAlumno + "," + idDocente + ") "
                + "VALUES ('" + tipoF + "','" + numF + "','" + montoF + "','" + concept + "'," + idAlu + "," + idDoc + ")";
        ejecutarSQL(sql);
    }
    
    public ResultSet listaAlumnosDocentes(){
         String sql="SELECT a.id_alumno,a.documento_alumno,a.nombre_alumno,a.apellido_alumno,d.id_docente,d.documento_docente,d.nombre_docente,d.apellido_docente "
                 + "FROM alumnos a, docentes d "
                 + "WHERE a.id_alumno > 0 AND d.id_docente > 0";
         return procesarSQL(sql);
    }
    
    public void actualizar(int id, String tipoF, String numF, String montoF, String concept, int idAlu, int idDoc){
        String sql = "UPDATE " + tabla + " SET " + tipo + "='" + tipoF + "', " + numFac + "='" + numF + "', " 
                + monto + "='" + montoF +  "', " + concepto + "='" + concept +  "', " 
                + idAlumno + "='" + idAlu +  "', " + idDocente + "='" + idDoc 
                + "' WHERE "+this.id+"="+id;
        ejecutarSQL(sql);
    }
    
    public void delete(int id){
        String sql = "DELETE FROM " + tabla + " WHERE "+this.id+"='"+id+"'";
        ejecutarSQL(sql);
    }
    
     private ResultSet procesarSQL(String sql){
        try {
            Statement st = (Statement) baseDatos.createStatement();
            ResultSet rs = st.executeQuery(sql);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionCarreras.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
     private void ejecutarSQL(String sql){
        try{
            PreparedStatement pst = (PreparedStatement) baseDatos.prepareStatement(sql);
            pst.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     }
}
