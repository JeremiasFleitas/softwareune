/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiones;

//import java.sql.Connection;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
//import java.sql.DriverManager;

/**
 *
 * @author Fernando
 */
public class Conexion {
     
    private static String driver = "com.mysql.cj.jdbc.Driver";
    private static String user = "root";
    private static String pass = "";
    private static String url= "jdbc:mysql://localhost:3306/db_une";
    private static String host="localhost";
    private static String port="3306";
    private static String db_name="db_une";
    private static String username="root";
    private static String password="";
               
    public static Connection conexion(){
        Connection connection = null;
        
        try{
            Class.forName(driver);
            connection = (Connection) DriverManager.getConnection(url,user,pass);
            if (connection != null) {
                System.out.println("Connection OK");
            }else{
                System.out.println("Connection failed");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return connection;
    }
    
    public static void main(String []args){
        Connection cn = Conexion.conexion(); 
    }
   
}
