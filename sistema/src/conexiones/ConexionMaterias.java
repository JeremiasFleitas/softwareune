/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiones;




import clasesIniciales.Materia;
import conexiones.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando
 */
public class ConexionMaterias {
   
    public Connection baseDatos = Conexion.conexion();
    //Nombre de las columnas
    String id="id_materia";
    String nombre="nombre_materia";
    String descripcion="descripcion_materia";
    String tabla = "materias";
    
    public ResultSet buscarPor(String parametro, String valor){
        String sql="";
        if (parametro.toLowerCase().equals("nombre")){
            sql="SELECT * FROM " + tabla + " WHERE " + nombre + " LIKE '%" + valor + "%'";
        }else if (parametro.toLowerCase().equals("descripcion")){
            sql="SELECT * FROM " + tabla + " WHERE " + descripcion + " LIKE '%" + valor + "%'";
        }else if (parametro.toLowerCase().equals("id")){
             sql = "SELECT * FROM  " + tabla + " WHERE " + id + "='" + valor + "'";
        }else{
            sql="SELECT * FROM " + tabla;
        }
        return procesarSQL(sql);
    }
    
    public void guardar(String nom, String descrip){
        String sql = "INSERT INTO " + tabla + "(" + nombre + "," + descripcion + ") "
                     + "VALUES ('" + nom + "','" + descrip + "')"; 
        System.out.println(sql);
        ejecutarSQL(sql);
    }
    
    public void actualizar(int id, String nom, String descrip){
        String sql = "UPDATE " + tabla + " SET " + nombre + "='" + nom + "', " + descripcion + "='" + descrip + "' WHERE "+this.id+"="+id;
        ejecutarSQL(sql);
    }
    
    public void delete(int id){
        String sql = "DELETE FROM " + tabla + " WHERE " + this.id + "=" + id;
        ejecutarSQL(sql);
    }
    
     private ResultSet procesarSQL(String sql){
        try {
            Statement st = (Statement) baseDatos.createStatement();
            ResultSet rs = st.executeQuery(sql);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionMaterias.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
     private void ejecutarSQL(String sql){
        try{
            PreparedStatement pst = (PreparedStatement) baseDatos.prepareStatement(sql);
            pst.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     } 
     
     
     public ArrayList<Materia> getListaMaterias(){
        ArrayList<Materia> listaMaterias = new ArrayList();
        Materia mater = null;
        Statement consulta;
        ResultSet resultado;
        try {
            consulta = (Statement) baseDatos.createStatement();
            resultado = consulta.executeQuery("SELECT * FROM materias");
            while(resultado.next()){
                mater = new Materia();
                mater.setId(resultado.getInt("id_materia"));
                mater.setNombre(resultado.getString("nombre_materia"));
                listaMaterias.add(mater);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionCarreras.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaMaterias;
    }
      
  
}
