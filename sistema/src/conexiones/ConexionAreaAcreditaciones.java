/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiones;

import clasesIniciales.Especialidad;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jeremias
 */
public class ConexionAreaAcreditaciones {
      public Connection baseDatos = Conexion.conexion();
    //Nombre de las columnas
    String id="id_area_acreditacion";
    String nombre="nombre_area";
    String tabla = "area_acreditaciones";
    

     public ResultSet buscarPor(String parametro, String valor){
        String sql="";
        sql = "SELECT nombre_area FROM  area_acreditaciones";
            //+ " WHERE nombre_area = '" + valor + "'"
        return procesarSQL(sql);
    } 

public void guardar(String nom) {
        String sql = "INSERT INTO area_acreditaciones(nombre_area) "
                     + "VALUES ( '" + nom + "')"; 
        System.out.println(sql); 
       ejecutarSQL(sql);
     }
    
    public void actualizar(int id, String area){

        String sql = "UPDATE " + tabla + " SET " + nombre + "='" + area + "' WHERE "+this.id+"="+id;
        ejecutarSQL(sql);
    }
    
    public void delete(int id){

        String sql = "DELETE FROM " + tabla + " WHERE "+this.id+"='"+id+"'";

        ejecutarSQL(sql);
    }
    
     private ResultSet procesarSQL(String sql){
        try {
            Statement st = (Statement) baseDatos.createStatement();
            ResultSet rs = st.executeQuery(sql);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionAreaAcreditaciones.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
     private void ejecutarSQL(String sql){
        try{
            PreparedStatement pst = (PreparedStatement) baseDatos.prepareStatement(sql);
            pst.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     }      
    
    public ArrayList getListaAreas(){
        ArrayList listaAreas = new ArrayList();
        Especialidad area = null;
        Statement consulta;
        ResultSet resultado;
        try {
            consulta = (Statement) baseDatos.createStatement();
            resultado = consulta.executeQuery("SELECT * FROM area_acreditaciones");
            while(resultado.next()){
                area = new Especialidad();
                area.setId(resultado.getInt("id_area_acreditacion"));
                area.setNombres(resultado.getString("nombre_area"));
                listaAreas.add(area);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionAreaAcreditaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaAreas;
    }
    
}
		
