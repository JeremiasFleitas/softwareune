/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiones;

import clasesIniciales.Alumno;
import clasesIniciales.Materia;
import conexiones.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jeremías
 */
public class ConexionAsistencias {
    public Connection baseDatos = Conexion.conexion();
    //Nombre de las columnas
    String id="id_asistencia";
    String fecha="fecha_asistencia";
    String asistencia="presencia_asistencia";
    String id_al_ma_pe="id_alumno_materia_periodo";
    String tabla="asistencias";
    Conexion cc= new Conexion ();
    
    
    public ResultSet buscarPor(String parametro, String valor){
        String sql="";
        
        sql = " SELECT id_asistencia, asis.fecha_asistencia, c.nombre_carrera, m.nombre_materia, a.nombre_alumno, a.apellido_alumno, asis.presencia_asistencia "
               + " FROM alumnos a, materias m,alumnos_materias_periodo amp,carreras c, materias_periodo mp, asistencias asis " 
               + " WHERE amp.id_alumno = a.id_alumno "
               + " AND mp.id_materia_periodo = amp.id_materia_periodo "
               + " AND mp.id_carrera = c.id_carrera ";
                
                   
                           
  if (parametro.toLowerCase().equals("fecha")){
            sql = sql.concat(" AND fecha_asistencia LIKE '%"+valor+"%'");
        }else if (parametro.toLowerCase().equals("asistencia")){
            sql = sql.concat(" AND presencia_asistencia LIKE '%"+valor+"%'");
        }
        
        return procesarSQL(sql);
    }
  
    public int devolverIdAlumnoMateriaPeriodo(int car, int mat, int al){
        String sql="";
        sql  = " SELECT * FROM alumnos_materias_periodo amp, materias_periodo mp " 
            + " WHERE amp.id_materia_periodo = mp.id_materia_periodo " 
            + " AND mp.id_carrera = " + car 
            + " AND mp.id_materia = " + mat 
            + " AND amp.id_alumno = " + al ;
         ResultSet rs = procesarSQL(sql);
        try {
            rs.next();
            int id = rs.getInt("id_alumno_materia_periodo");
             return id;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionAsistencias.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
     
    public void guardar(Date fec, int car, int mat, int al, String Asis) {
        int id = devolverIdAlumnoMateriaPeriodo(car, mat, al);
        String sql = "INSERT INTO asistencias(fecha_asistencia, presencia_asistencia, id_alumno_materia_periodo) "
                     + "VALUES ('" + fec + "','" + Asis + "', '" + id + "')"; 
        System.out.println(sql); 
       ejecutarSQL(sql);
     }
    
    public void actualizar(int id, Date fec, int car, int mat, int al, String Asis){
        String sql = "UPDATE " + tabla + " SET " + fecha + "='" + fec + "', " + asistencia + "='" + car + "', " + asistencia + "='" + mat + "', " + asistencia + "='" + al + "', " + asistencia + "='" + Asis + "' WHERE "+this.id+"="+id;
        ejecutarSQL(sql);
    }
    
    public void delete(int id){

        String sql = "DELETE FROM " + tabla + " WHERE "+this.id+"='"+id+"'";

        ejecutarSQL(sql);
    }
    
     private ResultSet procesarSQL(String sql){
        try {
            Statement st = (Statement) baseDatos.createStatement();
            ResultSet rs = st.executeQuery(sql);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionDocentes.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
     private void ejecutarSQL(String sql){
        try{
            PreparedStatement pst = (PreparedStatement) baseDatos.prepareStatement(sql);
            pst.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     } 
}

  