/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiones;


import clasesIniciales.Docente;
import clasesIniciales.Materia;
import conexiones.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
<<<<<<< HEAD
 * @author Fernando
 */
   
 /* @author Jeremias
 */
public class ConexionDocentes {
    
    public Connection baseDatos = Conexion.conexion();
    //Nombre de las columnas
    String id="id_docente";
    String nombre="nombre_docente";
    String apellido="apellido_docente";
    String numDoc="documento_docente";
    String tipoDoc="tipo_doc_docente";
    String contacto="contacto_docente";
    String email="email_docente";
    String tabla="docentes";

    
    public ResultSet buscarPor(String parametro, String valor){
        String sql="";
        if (parametro.toLowerCase().equals("nombre")){
            sql="SELECT * FROM docentes WHERE "+nombre+" LIKE '%"+valor+"%'"+" AND NOT id_docente = 0";
        }else if (parametro.toLowerCase().equals("apellido")){
            sql="SELECT * FROM docentes WHERE "+apellido+" LIKE '%"+valor+"%'"+" AND NOT id_docente = 0";
        }else if (parametro.toLowerCase().equals("documento")){
            sql="SELECT * FROM docentes WHERE "+numDoc+" LIKE '%"+valor+"%'"+" AND NOT id_docente = 0";
        }else if (parametro.toLowerCase().equals("tipoDoc")){
            sql="SELECT * FROM docentes WHERE "+tipoDoc+" LIKE '%"+valor+"%'"+" AND NOT id_docente = 0";
        }else if (parametro.toLowerCase().equals("contacto")){
            sql="SELECT * FROM docentes WHERE "+contacto+" LIKE '%"+valor+"%'"+" AND NOT id_docente = 0";
        }else if (parametro.toLowerCase().equals("email")){
            sql="SELECT * FROM docentes WHERE "+email+" LIKE '%"+valor+"%'"+" AND NOT id_docente = 0";
        }else if (parametro.toLowerCase().equals("id")){
             sql = "SELECT * FROM docentes WHERE " + id + "='" + valor + "'"+" AND NOT id_docente = 0";
        }else{
            sql="SELECT * FROM docentes"+" WHERE NOT id_docente = 0";
        }
        return procesarSQL(sql);
    }
    
    public void guardar(String nom, String apell, String doc, String modeloDoc, String contact, String correo){

        String sql = "INSERT INTO docentes(" + nombre + "," + apellido + "," + numDoc + "," + tipoDoc + "," + contacto + "," + email + ") "

                     + "VALUES ('" + nom + "','" + apell + "','" + doc + "','" + modeloDoc + "','" + contact + "','" + correo + "')"; 
        System.out.println(sql);
        ejecutarSQL(sql);
    }
    
    public void actualizar(int id, String nom, String apell, String doc, String modeloDoc, String contact, String correo){

        String sql = "UPDATE " + tabla + " SET " + nombre + "='" + nom + "', " + apellido + "='" + apell + "', " + numDoc + "='" + doc + "', " 
                + tipoDoc + "='" + modeloDoc + "', " + contacto + "='" + contact + "', " + email + "='" + correo + "' WHERE "+this.id+"="+id;
        ejecutarSQL(sql);
    }
    
    public void delete(int id){

        String sql = "DELETE FROM " + tabla + " WHERE "+this.id+"='"+id+"'";

        ejecutarSQL(sql);
    }
    
     private ResultSet procesarSQL(String sql){
        try {
            Statement st = (Statement) baseDatos.createStatement();
            ResultSet rs = st.executeQuery(sql);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionDocentes.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
     private void ejecutarSQL(String sql){
        try{
            PreparedStatement pst = (PreparedStatement) baseDatos.prepareStatement(sql);
            pst.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     }   
   
    public ArrayList getListaDocentes(){
        ArrayList listaDocentes = new ArrayList();
        Docente docen = null;
        Statement consulta;
        ResultSet resultado;
        try {
            consulta = (Statement) baseDatos.createStatement();
            resultado = consulta.executeQuery("SELECT * FROM " + tabla + " WHERE NOT id_docente = 0");
            while(resultado.next()){
                docen = new Docente();
                docen.setId(resultado.getInt(id));
                docen.setNombres(resultado.getString(nombre));
                docen.setApellidos(resultado.getString(apellido));
                docen.setDocumento(resultado.getString(numDoc));
                listaDocentes.add(docen);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionCarreras.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaDocentes;
    } 
     

}
   
//}else if (parametro.toLowerCase().equals("id")){
           //  sql = "SELECT * FROM " + tabla + " WHERE " + id + "='" + valor + "'";