/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package interfaces;

import clasesIniciales.Carrera;
import conexiones.ConexionAlumnos;
import conexiones.ConexionAlumnosCarreras;
import conexiones.ConexionAlumnosMaterias;
import conexiones.ConexionCarreras;
import conexiones.ConexionMateriasCarreras;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;


public class ABM_Matriculacion extends javax.swing.JFrame {
 
    public ABM_Matriculacion() {
        initComponents();
        cargarDatos();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menu_alumnoCarrera = new javax.swing.JPopupMenu();
        menuIt_desmatricularCarrera = new javax.swing.JMenuItem();
        menu_estadoCarrera = new javax.swing.JMenu();
        menuIt_estadoCaActivo = new javax.swing.JMenuItem();
        menuIt_estadoCaInactivo = new javax.swing.JMenuItem();
        menuIt_estadoCaEgresado = new javax.swing.JMenuItem();
        menu_alumnoMateria = new javax.swing.JPopupMenu();
        menuIt_desmatricularMateria = new javax.swing.JMenuItem();
        menu_estadoMateria = new javax.swing.JMenu();
        menuIt_estadoMaActivo = new javax.swing.JMenuItem();
        menuIt_estadoMaInactivo = new javax.swing.JMenuItem();
        menuIt_estadoMaAprobado = new javax.swing.JMenuItem();
        menuIt_estadoMaReprobado = new javax.swing.JMenuItem();
        menu_materiaCarrera = new javax.swing.JPopupMenu();
        menu_estadoMateriaCarrera = new javax.swing.JMenu();
        menuIt_activo = new javax.swing.JMenuItem();
        menuIt_inactivo = new javax.swing.JMenuItem();
        panel_general = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        table_alumnos = new javax.swing.JTable();
        labelT_listaAlumnos = new javax.swing.JLabel();
        comboB_busqueda = new javax.swing.JComboBox<>();
        btn_buscar = new javax.swing.JButton();
        txtF_buscar = new javax.swing.JTextField();
        btn_crearAlumno = new javax.swing.JButton();
        btn_matricular = new javax.swing.JButton();
        labelT_matricularAlumno = new javax.swing.JLabel();
        labelD_carrera = new javax.swing.JLabel();
        comboB_carrera = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        table_materiasPeriodo = new javax.swing.JTable();
        labelD_filtro = new javax.swing.JLabel();
        comboB_filtro = new javax.swing.JComboBox<>();
        panel_infoAlumno = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        table_alumnosMaterias = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        table_alumnosCarreras = new javax.swing.JTable();
        labelT_materiasAlumno = new javax.swing.JLabel();
        labelT_carreraAlumno = new javax.swing.JLabel();
        btn_crearMatCarr = new javax.swing.JButton();

        menuIt_desmatricularCarrera.setText("Desmatricular");
        menuIt_desmatricularCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_desmatricularCarreraActionPerformed(evt);
            }
        });
        menu_alumnoCarrera.add(menuIt_desmatricularCarrera);

        menu_estadoCarrera.setText("Estado");

        menuIt_estadoCaActivo.setText("Activo");
        menuIt_estadoCaActivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_estadoCaActivoActionPerformed(evt);
            }
        });
        menu_estadoCarrera.add(menuIt_estadoCaActivo);

        menuIt_estadoCaInactivo.setText("Inactivo");
        menuIt_estadoCaInactivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_estadoCaInactivoActionPerformed(evt);
            }
        });
        menu_estadoCarrera.add(menuIt_estadoCaInactivo);

        menuIt_estadoCaEgresado.setText("Egresado");
        menuIt_estadoCaEgresado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_estadoCaEgresadoActionPerformed(evt);
            }
        });
        menu_estadoCarrera.add(menuIt_estadoCaEgresado);

        menu_alumnoCarrera.add(menu_estadoCarrera);

        menuIt_desmatricularMateria.setText("Desmatricular");
        menuIt_desmatricularMateria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_desmatricularMateriaActionPerformed(evt);
            }
        });
        menu_alumnoMateria.add(menuIt_desmatricularMateria);

        menu_estadoMateria.setText("Estado");

        menuIt_estadoMaActivo.setText("Activo");
        menuIt_estadoMaActivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_estadoMaActivoActionPerformed(evt);
            }
        });
        menu_estadoMateria.add(menuIt_estadoMaActivo);

        menuIt_estadoMaInactivo.setText("Inactivo");
        menuIt_estadoMaInactivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_estadoMaInactivoActionPerformed(evt);
            }
        });
        menu_estadoMateria.add(menuIt_estadoMaInactivo);

        menuIt_estadoMaAprobado.setText("Aprobado");
        menuIt_estadoMaAprobado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_estadoMaAprobadoActionPerformed(evt);
            }
        });
        menu_estadoMateria.add(menuIt_estadoMaAprobado);

        menuIt_estadoMaReprobado.setText("Reprobado");
        menuIt_estadoMaReprobado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_estadoMaReprobadoActionPerformed(evt);
            }
        });
        menu_estadoMateria.add(menuIt_estadoMaReprobado);

        menu_alumnoMateria.add(menu_estadoMateria);

        menu_estadoMateriaCarrera.setText("Estado");

        menuIt_activo.setText("Activo");
        menuIt_activo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_activoActionPerformed(evt);
            }
        });
        menu_estadoMateriaCarrera.add(menuIt_activo);

        menuIt_inactivo.setText("Inactivo");
        menuIt_inactivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_inactivoActionPerformed(evt);
            }
        });
        menu_estadoMateriaCarrera.add(menuIt_inactivo);

        menu_materiaCarrera.add(menu_estadoMateriaCarrera);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
                formWindowGainedFocus(evt);
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
            }
        });

        panel_general.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        table_alumnos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nombre", "Documento"
            }
        ));
        table_alumnos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                table_alumnosMousePressed(evt);
            }
        });
        table_alumnos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                table_alumnosKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(table_alumnos);

        labelT_listaAlumnos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelT_listaAlumnos.setText("Lista de Alumnos");

        comboB_busqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Todo", "Nombre", "Apellido", "Documento", "Contacto", "Email" }));

        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        txtF_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtF_buscarActionPerformed(evt);
            }
        });

        btn_crearAlumno.setText("Crear Alumno");
        btn_crearAlumno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_crearAlumnoActionPerformed(evt);
            }
        });

        btn_matricular.setText("Matricular");
        btn_matricular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_matricularActionPerformed(evt);
            }
        });

        labelT_matricularAlumno.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelT_matricularAlumno.setText("Matricular Alumno");

        labelD_carrera.setText("Carrera:");

        comboB_carrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboB_carreraActionPerformed(evt);
            }
        });

        table_materiasPeriodo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Inicio", "Fin", "Hs/Semana", "Docente", "Estado"
            }
        ));
        table_materiasPeriodo.setComponentPopupMenu(menu_materiaCarrera);
        table_materiasPeriodo.setDoubleBuffered(true);
        jScrollPane1.setViewportView(table_materiasPeriodo);

        labelD_filtro.setText("Filtro:");

        comboB_filtro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Todos", "Activo", "Inactivo" }));
        comboB_filtro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboB_filtroActionPerformed(evt);
            }
        });

        panel_infoAlumno.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        table_alumnosMaterias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Contador", "Nombre", "Estado"
            }
        ));
        table_alumnosMaterias.setComponentPopupMenu(menu_alumnoMateria);
        jScrollPane5.setViewportView(table_alumnosMaterias);

        table_alumnosCarreras.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nombre", "Estado"
            }
        ));
        table_alumnosCarreras.setComponentPopupMenu(menu_alumnoCarrera);
        table_alumnosCarreras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                table_alumnosCarrerasMousePressed(evt);
            }
        });
        table_alumnosCarreras.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                table_alumnosCarrerasKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(table_alumnosCarreras);

        labelT_materiasAlumno.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelT_materiasAlumno.setText("Materias de la Carrera");

        labelT_carreraAlumno.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelT_carreraAlumno.setText("Carreras del Alumno");

        javax.swing.GroupLayout panel_infoAlumnoLayout = new javax.swing.GroupLayout(panel_infoAlumno);
        panel_infoAlumno.setLayout(panel_infoAlumnoLayout);
        panel_infoAlumnoLayout.setHorizontalGroup(
            panel_infoAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_infoAlumnoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_infoAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelT_carreraAlumno)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(panel_infoAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_infoAlumnoLayout.createSequentialGroup()
                        .addComponent(labelT_materiasAlumno)
                        .addGap(0, 401, Short.MAX_VALUE))
                    .addGroup(panel_infoAlumnoLayout.createSequentialGroup()
                        .addComponent(jScrollPane5)
                        .addContainerGap())))
        );
        panel_infoAlumnoLayout.setVerticalGroup(
            panel_infoAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_infoAlumnoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_infoAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelT_materiasAlumno, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelT_carreraAlumno))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_infoAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        btn_crearMatCarr.setText("Crear Materias y Carreras");
        btn_crearMatCarr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_crearMatCarrActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_generalLayout = new javax.swing.GroupLayout(panel_general);
        panel_general.setLayout(panel_generalLayout);
        panel_generalLayout.setHorizontalGroup(
            panel_generalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_generalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_generalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panel_infoAlumno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panel_generalLayout.createSequentialGroup()
                        .addGroup(panel_generalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelT_listaAlumnos)
                            .addComponent(btn_crearAlumno, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_generalLayout.createSequentialGroup()
                                .addComponent(comboB_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtF_buscar, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panel_generalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 533, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_generalLayout.createSequentialGroup()
                                .addComponent(btn_crearMatCarr)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_matricular))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_generalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(labelT_matricularAlumno)
                                .addGroup(panel_generalLayout.createSequentialGroup()
                                    .addComponent(labelD_carrera)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(comboB_carrera, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(labelD_filtro)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(comboB_filtro, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );
        panel_generalLayout.setVerticalGroup(
            panel_generalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_generalLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(panel_generalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelT_listaAlumnos)
                    .addComponent(labelT_matricularAlumno))
                .addGap(18, 18, 18)
                .addGroup(panel_generalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboB_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_buscar)
                    .addComponent(txtF_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelD_carrera)
                    .addComponent(comboB_carrera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelD_filtro)
                    .addComponent(comboB_filtro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_generalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(17, 17, 17)
                .addGroup(panel_generalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_crearAlumno, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_generalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_matricular)
                        .addComponent(btn_crearMatCarr)))
                .addGap(10, 10, 10)
                .addComponent(panel_infoAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel_general, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel_general, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void table_alumnosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_alumnosMousePressed
        int filaSeleccionada = table_alumnos.getSelectedRow();
        int idAl = Integer.parseInt(table_alumnos.getModel().getValueAt(filaSeleccionada, 1).toString());
        mostrarTablaAlumnosCarreras(idAl);
        mostrarTablaAlumnosMaterias(0,0);
    }//GEN-LAST:event_table_alumnosMousePressed

    private void table_alumnosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_table_alumnosKeyReleased
        int filaSeleccionada = table_alumnos.getSelectedRow();
        int idAl = Integer.parseInt(table_alumnos.getModel().getValueAt(filaSeleccionada, 1).toString());
        mostrarTablaAlumnosCarreras(idAl);
        mostrarTablaAlumnosMaterias(0,0);
    }//GEN-LAST:event_table_alumnosKeyReleased

    private void comboB_carreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboB_carreraActionPerformed
        Carrera c = (Carrera) comboB_carrera.getSelectedItem();
        String valor2 = (String) comboB_filtro.getSelectedItem();
        mostrarTablaMateriasPeriodo("idcarrera",String.valueOf(c.getId()),"estado",valor2); 
    }//GEN-LAST:event_comboB_carreraActionPerformed

    private void comboB_filtroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboB_filtroActionPerformed
        comboB_carreraActionPerformed(evt);
    }//GEN-LAST:event_comboB_filtroActionPerformed

    private void btn_matricularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_matricularActionPerformed
        //Primero tengo que filtrar si ya existe la carrera para no volver a crearla
        
        
        int filaSeleccionada = table_alumnos.getSelectedRow();
        int idAlumno = Integer.parseInt(table_alumnos.getModel().getValueAt(filaSeleccionada, 1).toString());
        
        filaSeleccionada = table_materiasPeriodo.getSelectedRow();
        int idCarrera =  Integer.parseInt(table_materiasPeriodo.getModel().getValueAt(filaSeleccionada, 6).toString());
        int idMateriaPeriodo =  Integer.parseInt(table_materiasPeriodo.getModel().getValueAt(filaSeleccionada, 1).toString());
        
        ResultSet rs = baseDatosAlumnoCarrera.buscarPor2(String.valueOf(idCarrera),String.valueOf(idAlumno));
        System.out.println("Id Carrera = " +idCarrera);
        System.out.println("Id Alumno = "+idAlumno);
        
        
        try {
            if (!(rs.next())){
                baseDatosAlumnoCarrera.guardar("Activo", String.valueOf(idAlumno), String.valueOf(idCarrera));
                mostrarTablaAlumnosCarreras(idAlumno);
            }
            //Luego tengo que crear si o si un registro en alumnos_materias_periodo
        } catch (SQLException ex) {
            Logger.getLogger(ABM_Matriculacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        baseDatosAlumnoMateria.guardar("Activo", String.valueOf(idAlumno), String.valueOf(idMateriaPeriodo));
        
        mostrarTablaAlumnosMaterias(idAlumno, idCarrera);
        
        
    }//GEN-LAST:event_btn_matricularActionPerformed

    private void table_alumnosCarrerasMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_alumnosCarrerasMousePressed
        int filaSeleccionada = table_alumnosCarreras.getSelectedRow();
        int idCa = Integer.parseInt(table_alumnosCarreras.getModel().getValueAt(filaSeleccionada, 2).toString());
        int idAl = Integer.parseInt(table_alumnosCarreras.getModel().getValueAt(filaSeleccionada, 5).toString());
        mostrarTablaAlumnosMaterias(idAl,idCa);
    }//GEN-LAST:event_table_alumnosCarrerasMousePressed

    private void table_alumnosCarrerasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_table_alumnosCarrerasKeyReleased
        int filaSeleccionada = table_alumnosCarreras.getSelectedRow();
        int idCa = Integer.parseInt(table_alumnosCarreras.getModel().getValueAt(filaSeleccionada, 2).toString());
        int idAl = Integer.parseInt(table_alumnosCarreras.getModel().getValueAt(filaSeleccionada, 5).toString());
        mostrarTablaAlumnosMaterias(idAl,idCa);
    }//GEN-LAST:event_table_alumnosCarrerasKeyReleased

    private void menuIt_desmatricularMateriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_desmatricularMateriaActionPerformed
        int filaSeleccionada = table_alumnosMaterias.getSelectedRow();
        String idAlumno = table_alumnosMaterias.getModel().getValueAt(filaSeleccionada, 7).toString();
        String idCarrera = table_alumnosMaterias.getModel().getValueAt(filaSeleccionada, 2).toString();
        baseDatosAlumnoMateria.delete(table_alumnosMaterias.getModel().getValueAt(filaSeleccionada, 1).toString());
        mostrarTablaAlumnosMaterias(Integer.parseInt(idAlumno),Integer.parseInt(idCarrera));
    }//GEN-LAST:event_menuIt_desmatricularMateriaActionPerformed

    private void menuIt_desmatricularCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_desmatricularCarreraActionPerformed
        try {
            int filaSeleccionada = table_alumnosCarreras.getSelectedRow();
            int idA = Integer.parseInt(table_alumnosCarreras.getModel().getValueAt(filaSeleccionada, 5).toString());
            int idC = Integer.parseInt(table_alumnosCarreras.getModel().getValueAt(filaSeleccionada, 2).toString());
            baseDatosAlumnoCarrera.delete(table_alumnosCarreras.getModel().getValueAt(filaSeleccionada, 1).toString());
            
            ResultSet rs = baseDatosMateriaCarrera.buscarPor("idcarrera", String.valueOf(idC));
            
            while(rs.next()){
                baseDatosAlumnoMateria.deleteAll(rs.getString(1));
            }
            
            mostrarTablaAlumnosCarreras(idA);
            mostrarTablaAlumnosMaterias(0,0);
           
        } catch (SQLException ex) {
            Logger.getLogger(ABM_Matriculacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_menuIt_desmatricularCarreraActionPerformed

    private void menuIt_estadoMaActivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_estadoMaActivoActionPerformed
        modificarEstadoMateria("Activo");
    }//GEN-LAST:event_menuIt_estadoMaActivoActionPerformed

    private void menuIt_estadoMaInactivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_estadoMaInactivoActionPerformed
        modificarEstadoMateria("Inactivo");
    }//GEN-LAST:event_menuIt_estadoMaInactivoActionPerformed

    private void menuIt_estadoMaAprobadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_estadoMaAprobadoActionPerformed
        modificarEstadoMateria("Aprobado");
    }//GEN-LAST:event_menuIt_estadoMaAprobadoActionPerformed

    private void menuIt_estadoMaReprobadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_estadoMaReprobadoActionPerformed
        modificarEstadoMateria("Reprobado");
    }//GEN-LAST:event_menuIt_estadoMaReprobadoActionPerformed

    private void menuIt_estadoCaActivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_estadoCaActivoActionPerformed
        modificarEstadoCarrera("Activo"); 
    }//GEN-LAST:event_menuIt_estadoCaActivoActionPerformed

    private void menuIt_estadoCaInactivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_estadoCaInactivoActionPerformed
        modificarEstadoCarrera("Inactivo");
    }//GEN-LAST:event_menuIt_estadoCaInactivoActionPerformed

    private void menuIt_estadoCaEgresadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_estadoCaEgresadoActionPerformed
        modificarEstadoCarrera("Egresado");
    }//GEN-LAST:event_menuIt_estadoCaEgresadoActionPerformed

    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        String valor = txtF_buscar.getText();
        String parametro = comboB_busqueda.getSelectedItem().toString();
        mostrarTablaAlumno(parametro,valor);
    }//GEN-LAST:event_btn_buscarActionPerformed

    private void txtF_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtF_buscarActionPerformed
        btn_buscarActionPerformed(evt);
    }//GEN-LAST:event_txtF_buscarActionPerformed

    private void btn_crearAlumnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_crearAlumnoActionPerformed
        ABM_Alumnos alumnos = new ABM_Alumnos();
        alumnos.setVisible(true);
    }//GEN-LAST:event_btn_crearAlumnoActionPerformed

    private void btn_crearMatCarrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_crearMatCarrActionPerformed
        ABM_Carreras_Materias carrMat = new ABM_Carreras_Materias();
        carrMat.setVisible(true);
    }//GEN-LAST:event_btn_crearMatCarrActionPerformed

    private void formWindowGainedFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowGainedFocus
        mostrarTablaAlumno("","");
        String valor2 = (String) comboB_filtro.getSelectedItem();
        mostrarTablaMateriasPeriodo("idcarrera",String.valueOf(comboB_carrera.getItemAt(0).getId()),"estado",valor2);
    }//GEN-LAST:event_formWindowGainedFocus

    private void menuIt_activoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_activoActionPerformed
        modificarEstadoMateriaCarrera("Activo");
    }//GEN-LAST:event_menuIt_activoActionPerformed

    private void menuIt_inactivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_inactivoActionPerformed
        modificarEstadoMateriaCarrera("Inactivo");
    }//GEN-LAST:event_menuIt_inactivoActionPerformed
    
    
    //FUNCIONES PROPIAS
    private void cargarDatos(){
        mostrarTablaAlumno("","");
        mostrarListaCarreras();
        String valor2 = (String) comboB_filtro.getSelectedItem();
        mostrarTablaMateriasPeriodo("idcarrera",String.valueOf(comboB_carrera.getItemAt(0).getId()),"estado",valor2);   
    }
    
    private void mostrarTablaAlumno(String parametro, String valor){    
        DefaultTableModel modelo = new DefaultTableModel();     //establecer modelo de la tabla
        modelo.addColumn("Contador");
        modelo.addColumn("Id");
        modelo.addColumn("Nombre");
        modelo.addColumn("N° Doc.");
        modelo.addColumn("Tipo Doc.");
        modelo.addColumn("Contacto");
        modelo.addColumn("Email");
        
        String[] datos = new String[7];                         //cargar los datos a la tabla
        try{
            
            ResultSet rs = baseDatosAlumno.buscarPor(parametro,valor);
            int i = 1;
            
            while(rs.next()){
                
                datos[0] = String.valueOf(i);                                   //Contador
                datos[1] = rs.getString(1);                                     //Id
                datos[2] = rs.getString(2) + " " + rs.getString(3);             //Nombres
                datos[3] = rs.getString(4);                                     //Documento
                datos[4] = rs.getString(5);                                     //Tipo Documento
                datos[5] = rs.getString(6);                                     //Contacto
                datos[6] = rs.getString(7);                                     //Email
                      
                modelo.addRow(datos);
                i++;
            }
            
            table_alumnos.setModel(modelo);
            TableColumnModel tcm = table_alumnos.getColumnModel();
            tcm.removeColumn(tcm.getColumn(1)); //id
            tcm.removeColumn(tcm.getColumn(3)); //tipo documento
            tcm.removeColumn(tcm.getColumn(3)); //contacto
            tcm.removeColumn(tcm.getColumn(3)); //email
            
        }catch(SQLException ex){
            System.out.println("Algo falló"+ex);
        }
    }
    
    private void mostrarTablaMateriasPeriodo(String parametro, String valor, String parametro2, String valor2){
       
        DefaultTableModel modelo = new DefaultTableModel();     //establecer modelo de la tabla
        
        modelo.addColumn("Contador");
        modelo.addColumn("IdMateriaPeriodo");
        modelo.addColumn("IdMateria");
        modelo.addColumn("Materia");
        modelo.addColumn("IdDocente");
        modelo.addColumn("Docente");
        modelo.addColumn("IdCarrera");
        modelo.addColumn("Carrera");
        modelo.addColumn("Hs/S");
        modelo.addColumn("Inicio");
        modelo.addColumn("Fin");
        modelo.addColumn("Estado");
   
        String[] datos = new String[12];
        
        try{
            
            ResultSet rs = baseDatosMateriaCarrera.buscarPor2(parametro, valor, parametro2, valor2);
            int i = 1;
            
            while(rs.next()){
                
                datos[0] = String.valueOf(i);                         //Contador
                datos[1] = rs.getString(1);                           //IdMateriaPeriodo
                datos[2] = rs.getString(2);                           //IdMateria
                datos[3] = rs.getString(3);                           //Materia
                datos[4] = rs.getString(4);                           //IdDocente
                datos[5] = rs.getString(5) + " " + rs.getString(6);   //Nombre Docente + Apellido Docente
                datos[6] = rs.getString(7);                           //IdCarrera
                datos[7] = rs.getString(8);                           //Carrera
                datos[8] = rs.getString(9);                           //Hs/S
                datos[9] = rs.getString(10);                          //Inicio
                datos[10] = rs.getString(11);                         //Fin
                datos[11] = rs.getString(12);                         //Estado Materia
                
                modelo.addRow(datos);
                i++;
            }
            
            table_materiasPeriodo.setModel(modelo);
            TableColumnModel tcm = table_materiasPeriodo.getColumnModel();
            
            tcm.removeColumn(tcm.getColumn(1)); //IdMateriaPeriodo
            tcm.removeColumn(tcm.getColumn(1)); //IdMateria
            tcm.removeColumn(tcm.getColumn(2)); //IdDocente
            tcm.removeColumn(tcm.getColumn(3)); //IdCarrera
            
        }catch(SQLException ex){
            System.out.println("Algo falló"+ex);
        } 
    }
    
    private void mostrarTablaAlumnosCarreras(int idAlumno){
        DefaultTableModel modelo = new DefaultTableModel();     
        
        modelo.addColumn("Contador");
        modelo.addColumn("Id");
        modelo.addColumn("IdCarrera");
        modelo.addColumn("Nombre");
        modelo.addColumn("Estado");
        modelo.addColumn("IdAlumno");
        
        String[] datos = new String[6];                       
        try{
            if(!(idAlumno==0)){
                ResultSet rs = baseDatosAlumnoCarrera.buscarPor("idAlumno", String.valueOf(idAlumno));
                int i = 1;

                while(rs.next()){

                    datos[0] = String.valueOf(i);                   //Contador
                    datos[1] = rs.getString(1);                     //Id
                    datos[2] = rs.getString(2);                     //IdCarrera
                    datos[3] = rs.getString(3);                     //Nombre
                    datos[4] = rs.getString(4);                     //Estado
                    datos[5] = rs.getString(5);                     //IdAlumno

                    modelo.addRow(datos);
                    i++;
                }
            }
           
            table_alumnosCarreras.setModel(modelo);
            TableColumnModel tcm = table_alumnosCarreras.getColumnModel();
            tcm.removeColumn(tcm.getColumn(1)); //Id
            tcm.removeColumn(tcm.getColumn(1)); //IdCarrera
            tcm.removeColumn(tcm.getColumn(3)); //IdAlumno
            
        }catch(SQLException ex){
            System.out.println("Algo falló"+ex);
        }
        
    }
    
    private void mostrarTablaAlumnosMaterias(int idAlumno, int idCarrera){
        DefaultTableModel modelo = new DefaultTableModel();     
        
        modelo.addColumn("Contador");
        modelo.addColumn("Id");
        modelo.addColumn("IdCarrera");
        modelo.addColumn("Nombre Carr");
        modelo.addColumn("IdMateriaPeriodo");
        modelo.addColumn("IdMateria");
        modelo.addColumn("Nombre");
        modelo.addColumn("IdAlumno");
        modelo.addColumn("Estado");
        
        String[] datos = new String[9];
        
        try{
            if(!((idAlumno==0)&(idCarrera==0))){
                ResultSet rs = baseDatosAlumnoMateria.buscarPor(String.valueOf(idAlumno), String.valueOf(idCarrera));
                int i = 1;

                while(rs.next()){

                    datos[0] = String.valueOf(i);                   //Contador
                    datos[1] = rs.getString(1);                     //Id
                    datos[2] = rs.getString(2);                     //IdCarrera
                    datos[3] = rs.getString(3);                     //Nombre Carre
                    datos[4] = rs.getString(4);                     //IdMAteriaPeriodo
                    datos[5] = rs.getString(5);                     //IdMateria
                    datos[6] = rs.getString(6);                     //Nombre M.
                    datos[7] = rs.getString(7);                     //IdAlumno  
                    datos[8] = rs.getString(8);                     //Estado

                    modelo.addRow(datos);
                    i++;
                }
            }
       
            table_alumnosMaterias.setModel(modelo);
            TableColumnModel tcm = table_alumnosMaterias.getColumnModel();
            tcm.removeColumn(tcm.getColumn(1)); //Id
            tcm.removeColumn(tcm.getColumn(1)); //IdCarrera
            tcm.removeColumn(tcm.getColumn(1)); //Nombre Carr
            tcm.removeColumn(tcm.getColumn(1)); //IdMateriaPeriodo
            tcm.removeColumn(tcm.getColumn(1)); //IdMateria
            tcm.removeColumn(tcm.getColumn(2)); //IdAlumno
            
        }catch(SQLException ex){
            System.out.println("Algo falló"+ex);
        }
        
    }
    
    private void mostrarListaCarreras(){
        comboB_carrera.removeAllItems();
        listaCarreras = baseDatosCarrera.getListaCarreras();
        Iterator iterador = listaCarreras.iterator();
        while(iterador.hasNext()){
            Carrera carrera = (Carrera) iterador.next();
            comboB_carrera.addItem(carrera);
        }
        AutoCompleteDecorator.decorate(comboB_carrera); 
    }
    
    private void vaciarLista(){
        mostrarTablaAlumnosCarreras(0);
        mostrarTablaAlumnosMaterias(0,0);
    }
    
    private void modificarEstadoMateria(String estad){
        int filaSeleccionada = table_alumnosMaterias.getSelectedRow();
        String idAlumno = table_alumnosMaterias.getModel().getValueAt(filaSeleccionada, 7).toString();
        String idCarrera = table_alumnosMaterias.getModel().getValueAt(filaSeleccionada, 2).toString();
        baseDatosAlumnoMateria.actualizarEstado(table_alumnosMaterias.getModel().getValueAt(filaSeleccionada, 1).toString(), estad);
        mostrarTablaAlumnosMaterias(Integer.parseInt(idAlumno),Integer.parseInt(idCarrera));
    }
    
    private void modificarEstadoCarrera(String estad){
        int filaSeleccionada = table_alumnosCarreras.getSelectedRow();
        String idAlumno = table_alumnosCarreras.getModel().getValueAt(filaSeleccionada, 5).toString();
        baseDatosAlumnoCarrera.actualizarEstado(table_alumnosCarreras.getModel().getValueAt(filaSeleccionada, 1).toString(), estad);
        mostrarTablaAlumnosCarreras(Integer.parseInt(idAlumno));
    }
    
    private void modificarEstadoMateriaCarrera(String estad){
        int filaSeleccionada = table_materiasPeriodo.getSelectedRow();
        
        baseDatosMateriaCarrera.actualizarEstado(table_materiasPeriodo.getModel().getValueAt(filaSeleccionada, 1).toString(), estad);
        
        String valor2 = (String) comboB_filtro.getSelectedItem();
        Carrera c = (Carrera) comboB_carrera.getSelectedItem();
        mostrarTablaMateriasPeriodo("idcarrera",String.valueOf(c.getId()),"estado",valor2);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ABM_Matriculacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ABM_Matriculacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ABM_Matriculacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ABM_Matriculacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ABM_Matriculacion().setVisible(true);
            }
        });
    }
    
    private ConexionMateriasCarreras baseDatosMateriaCarrera = new ConexionMateriasCarreras();
    private ConexionCarreras baseDatosCarrera = new ConexionCarreras();
    private ConexionAlumnos baseDatosAlumno = new ConexionAlumnos();
    private ConexionAlumnosCarreras baseDatosAlumnoCarrera = new ConexionAlumnosCarreras();
    private ConexionAlumnosMaterias baseDatosAlumnoMateria = new ConexionAlumnosMaterias();
    private ArrayList listaCarreras;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_crearAlumno;
    private javax.swing.JButton btn_crearMatCarr;
    private javax.swing.JButton btn_matricular;
    private javax.swing.JComboBox<String> comboB_busqueda;
    private javax.swing.JComboBox<Carrera> comboB_carrera;
    private javax.swing.JComboBox<String> comboB_filtro;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel labelD_carrera;
    private javax.swing.JLabel labelD_filtro;
    private javax.swing.JLabel labelT_carreraAlumno;
    private javax.swing.JLabel labelT_listaAlumnos;
    private javax.swing.JLabel labelT_materiasAlumno;
    private javax.swing.JLabel labelT_matricularAlumno;
    private javax.swing.JMenuItem menuIt_activo;
    private javax.swing.JMenuItem menuIt_desmatricularCarrera;
    private javax.swing.JMenuItem menuIt_desmatricularMateria;
    private javax.swing.JMenuItem menuIt_estadoCaActivo;
    private javax.swing.JMenuItem menuIt_estadoCaEgresado;
    private javax.swing.JMenuItem menuIt_estadoCaInactivo;
    private javax.swing.JMenuItem menuIt_estadoMaActivo;
    private javax.swing.JMenuItem menuIt_estadoMaAprobado;
    private javax.swing.JMenuItem menuIt_estadoMaInactivo;
    private javax.swing.JMenuItem menuIt_estadoMaReprobado;
    private javax.swing.JMenuItem menuIt_inactivo;
    private javax.swing.JPopupMenu menu_alumnoCarrera;
    private javax.swing.JPopupMenu menu_alumnoMateria;
    private javax.swing.JMenu menu_estadoCarrera;
    private javax.swing.JMenu menu_estadoMateria;
    private javax.swing.JMenu menu_estadoMateriaCarrera;
    private javax.swing.JPopupMenu menu_materiaCarrera;
    private javax.swing.JPanel panel_general;
    private javax.swing.JPanel panel_infoAlumno;
    private javax.swing.JTable table_alumnos;
    private javax.swing.JTable table_alumnosCarreras;
    private javax.swing.JTable table_alumnosMaterias;
    private javax.swing.JTable table_materiasPeriodo;
    private javax.swing.JTextField txtF_buscar;
    // End of variables declaration//GEN-END:variables
}
