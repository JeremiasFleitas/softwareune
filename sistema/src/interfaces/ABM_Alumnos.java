package interfaces;

import conexiones.ConexionAlumnos;
import javax.swing.table.DefaultTableModel;
import com.mysql.jdbc.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Fernando
 */
public class ABM_Alumnos extends javax.swing.JFrame {

    //Constructor-------------------------------------------------------------------------------------------------------------------------------
    
    public ABM_Alumnos() {
        initComponents();
        mostrarDatos("","");
        this.setLocationRelativeTo(null);
    }
    
    //Código Generado---------------------------------------------------------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menu_opcionesTabla = new javax.swing.JPopupMenu();
        menuIt_editar = new javax.swing.JMenuItem();
        menuIt_borrar = new javax.swing.JMenuItem();
        panel_crearAlumno = new javax.swing.JPanel();
        labelD_nombre = new javax.swing.JLabel();
        labelD_numDoc = new javax.swing.JLabel();
        txtF_nombre = new javax.swing.JTextField();
        txtF_numDoc = new javax.swing.JTextField();
        labelD_contacto = new javax.swing.JLabel();
        txtF_email = new javax.swing.JTextField();
        labelD_apellido = new javax.swing.JLabel();
        labelD_email = new javax.swing.JLabel();
        txtF_apellido = new javax.swing.JTextField();
        txtF_contacto = new javax.swing.JTextField();
        labelT_crearAlumno = new javax.swing.JLabel();
        separator1 = new javax.swing.JSeparator();
        labelD_tipoDoc = new javax.swing.JLabel();
        comboB_tipoDoc = new javax.swing.JComboBox<>();
        panel_tablaAlumno = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_alumno = new javax.swing.JTable();
        panel_botones = new javax.swing.JPanel();
        btn_guardar = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        btn_editar = new javax.swing.JButton();
        panel_buscarAlumno = new javax.swing.JPanel();
        btn_buscar = new javax.swing.JButton();
        comboB_busqueda = new javax.swing.JComboBox<>();
        txtF_busqueda = new javax.swing.JTextField();
        labelT_buscarAlumno = new javax.swing.JLabel();
        separator2 = new javax.swing.JSeparator();

        menuIt_editar.setText("Editar");
        menuIt_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_editarActionPerformed(evt);
            }
        });
        menu_opcionesTabla.add(menuIt_editar);

        menuIt_borrar.setText("Borrar");
        menuIt_borrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_borrarActionPerformed(evt);
            }
        });
        menu_opcionesTabla.add(menuIt_borrar);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        panel_crearAlumno.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        labelD_nombre.setText("* Nombres:");

        labelD_numDoc.setText("* N° Doc.:");

        labelD_contacto.setText("Contacto:");

        labelD_apellido.setText("* Apellidos:");

        labelD_email.setText("E-mail:");

        labelT_crearAlumno.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelT_crearAlumno.setText("Crear Alumno");

        labelD_tipoDoc.setText("Tipo doc.");

        comboB_tipoDoc.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "N/E", "C.I.N.", "D.N.I.", "Pasaporte" }));

        javax.swing.GroupLayout panel_crearAlumnoLayout = new javax.swing.GroupLayout(panel_crearAlumno);
        panel_crearAlumno.setLayout(panel_crearAlumnoLayout);
        panel_crearAlumnoLayout.setHorizontalGroup(
            panel_crearAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_crearAlumnoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_crearAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panel_crearAlumnoLayout.createSequentialGroup()
                        .addComponent(labelT_crearAlumno)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(separator1, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_crearAlumnoLayout.createSequentialGroup()
                        .addGroup(panel_crearAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_crearAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_crearAlumnoLayout.createSequentialGroup()
                                    .addComponent(labelD_nombre)
                                    .addGap(18, 18, 18))
                                .addGroup(panel_crearAlumnoLayout.createSequentialGroup()
                                    .addComponent(labelD_tipoDoc)
                                    .addGap(20, 20, 20)))
                            .addGroup(panel_crearAlumnoLayout.createSequentialGroup()
                                .addComponent(labelD_contacto)
                                .addGap(18, 18, 18)))
                        .addGroup(panel_crearAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(comboB_tipoDoc, javax.swing.GroupLayout.Alignment.LEADING, 0, 185, Short.MAX_VALUE)
                            .addComponent(txtF_nombre, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtF_contacto))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panel_crearAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panel_crearAlumnoLayout.createSequentialGroup()
                                .addComponent(labelD_email)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtF_email, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel_crearAlumnoLayout.createSequentialGroup()
                                .addComponent(labelD_numDoc)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtF_numDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel_crearAlumnoLayout.createSequentialGroup()
                                .addComponent(labelD_apellido)
                                .addGap(18, 18, 18)
                                .addComponent(txtF_apellido, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(21, 21, 21))
        );
        panel_crearAlumnoLayout.setVerticalGroup(
            panel_crearAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_crearAlumnoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_crearAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelT_crearAlumno)
                    .addComponent(separator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panel_crearAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelD_nombre)
                    .addComponent(txtF_nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelD_apellido)
                    .addComponent(txtF_apellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_crearAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelD_numDoc)
                    .addComponent(labelD_tipoDoc)
                    .addComponent(comboB_tipoDoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtF_numDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_crearAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelD_contacto)
                    .addComponent(txtF_contacto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelD_email)
                    .addComponent(txtF_email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        panel_tablaAlumno.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        table_alumno.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        table_alumno.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Contador", "Id", "Nombres", "Apellidos", "N° Doc.", "Tipo Doc.", "Contacto", "Email"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table_alumno.setComponentPopupMenu(menu_opcionesTabla);
        jScrollPane2.setViewportView(table_alumno);
        table_alumno.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout panel_tablaAlumnoLayout = new javax.swing.GroupLayout(panel_tablaAlumno);
        panel_tablaAlumno.setLayout(panel_tablaAlumnoLayout);
        panel_tablaAlumnoLayout.setHorizontalGroup(
            panel_tablaAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_tablaAlumnoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        panel_tablaAlumnoLayout.setVerticalGroup(
            panel_tablaAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_tablaAlumnoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(162, 162, 162))
        );

        panel_botones.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        panel_botones.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_guardar.setText("Guardar");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });
        panel_botones.add(btn_guardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 85, -1));

        btn_cancelar.setText("Cancelar");
        btn_cancelar.setToolTipText("");
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });
        panel_botones.add(btn_cancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, 85, -1));

        btn_editar.setText("Editar");
        btn_editar.setToolTipText("");
        btn_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarActionPerformed(evt);
            }
        });
        panel_botones.add(btn_editar, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, 85, -1));

        panel_buscarAlumno.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        comboB_busqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mostrar Todo", "Nombre", "Apellido", "Documento", "Contacto", "Email" }));

        txtF_busqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtF_busquedaActionPerformed(evt);
            }
        });

        labelT_buscarAlumno.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelT_buscarAlumno.setText("Buscar Alumno");
        labelT_buscarAlumno.setMaximumSize(new java.awt.Dimension(95, 17));
        labelT_buscarAlumno.setMinimumSize(new java.awt.Dimension(95, 17));
        labelT_buscarAlumno.setPreferredSize(new java.awt.Dimension(95, 17));

        javax.swing.GroupLayout panel_buscarAlumnoLayout = new javax.swing.GroupLayout(panel_buscarAlumno);
        panel_buscarAlumno.setLayout(panel_buscarAlumnoLayout);
        panel_buscarAlumnoLayout.setHorizontalGroup(
            panel_buscarAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_buscarAlumnoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_buscarAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_buscarAlumnoLayout.createSequentialGroup()
                        .addComponent(comboB_busqueda, 0, 131, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtF_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 498, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_buscar)
                        .addContainerGap())
                    .addGroup(panel_buscarAlumnoLayout.createSequentialGroup()
                        .addComponent(labelT_buscarAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(separator2))))
        );
        panel_buscarAlumnoLayout.setVerticalGroup(
            panel_buscarAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_buscarAlumnoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_buscarAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelT_buscarAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(separator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panel_buscarAlumnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_buscar)
                    .addComponent(comboB_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtF_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panel_crearAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panel_botones, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE))
                    .addComponent(panel_buscarAlumno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel_tablaAlumno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panel_crearAlumno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel_botones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel_buscarAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel_tablaAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, 212, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    //Eventos-----------------------------------------------------------------------------------------------------------------------------------
    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed
        if((txtF_nombre.getText().equals(""))&&(txtF_apellido.getText().equals(""))&&(txtF_numDoc.getText().equals(""))){
           JOptionPane.showMessageDialog(rootPane, "¡Debes completar todos los campos obligatorios(*)!");
        }else{
            baseDatos.guardar(txtF_nombre.getText(), txtF_apellido.getText(),txtF_numDoc.getText(),
                    comboB_tipoDoc.getSelectedItem().toString(),txtF_contacto.getText(),txtF_email.getText());
            JOptionPane.showMessageDialog(rootPane, "¡Guardado con Éxito!");
            vaciarCampos();
            mostrarDatos("","");
        }
    }//GEN-LAST:event_btn_guardarActionPerformed

    private void menuIt_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_editarActionPerformed
        
        int filaSeleccionada = table_alumno.getSelectedRow();
        
        idEditar = Integer.parseInt(table_alumno.getModel().getValueAt(filaSeleccionada, 1).toString());
 
        txtF_nombre.setText(table_alumno.getValueAt(filaSeleccionada, 1).toString().trim());
        txtF_apellido.setText(table_alumno.getValueAt(filaSeleccionada, 2).toString().trim());
        txtF_numDoc.setText(table_alumno.getValueAt(filaSeleccionada, 3).toString().trim());
        
        String tipo_doc = table_alumno.getValueAt(filaSeleccionada, 4).toString().trim(); 
        
        if (tipo_doc.equals("N/E")){
            comboB_tipoDoc.setSelectedIndex(0);
        }else if(tipo_doc.equals("C.I.N.")){
            comboB_tipoDoc.setSelectedIndex(1);
        }else if(tipo_doc.equals("D.N.I.")){
            comboB_tipoDoc.setSelectedIndex(2);
        }else{
            comboB_tipoDoc.setSelectedIndex(3);
        }
        
        txtF_contacto.setText(table_alumno.getValueAt(filaSeleccionada, 5).toString().trim());
        txtF_email.setText(table_alumno.getValueAt(filaSeleccionada, 6).toString().trim());
       
        btn_editar.setEnabled(true);
        btn_guardar.setEnabled(false);
    }//GEN-LAST:event_menuIt_editarActionPerformed

    private void menuIt_borrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_borrarActionPerformed
        
        int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "¡Alerta!", JOptionPane.YES_NO_OPTION);
        
        if(resp==0){
            int filaSeleccionada = table_alumno.getSelectedRow();
            idEditar = Integer.parseInt(table_alumno.getModel().getValueAt(filaSeleccionada, 1).toString());
            baseDatos.delete(idEditar);
            JOptionPane.showMessageDialog(rootPane, "¡Borrado con Éxito!");
            mostrarDatos("","");
            idEditar = null;
        }         

    }//GEN-LAST:event_menuIt_borrarActionPerformed

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        if(btn_editar.isEnabled()){
            vaciarCampos();
            btn_guardar.setEnabled(true);
            btn_editar.setEnabled(false);
        }else{
            vaciarCampos();
        }
        
    }//GEN-LAST:event_btn_cancelarActionPerformed

    private void btn_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarActionPerformed
        baseDatos.actualizar(idEditar, txtF_nombre.getText(), txtF_apellido.getText(),txtF_numDoc.getText(),
                    comboB_tipoDoc.getSelectedItem().toString(),txtF_contacto.getText(),txtF_email.getText());
        
        vaciarCampos();
        mostrarDatos("","");
        
        JOptionPane.showMessageDialog(rootPane, "¡Editado con Éxito!");
        
        btn_editar.setEnabled(false);
        btn_guardar.setEnabled(true);
    }//GEN-LAST:event_btn_editarActionPerformed

    private void txtF_busquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtF_busquedaActionPerformed
        String valor = txtF_busqueda.getText();
        String parametro = comboB_busqueda.getSelectedItem().toString();
        mostrarDatos(parametro,valor);
    }//GEN-LAST:event_txtF_busquedaActionPerformed

    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        String valor = txtF_busqueda.getText();
        String parametro = comboB_busqueda.getSelectedItem().toString();
        mostrarDatos(parametro,valor);
    }//GEN-LAST:event_btn_buscarActionPerformed

    // Funciones y Procedimientos Propios----------------------------------------------------------------------------------------------------------
    
    void mostrarDatos(String parametro, String valor){     //trae los datos de acuerdo al parametro y al valor, mostrarDatos("","") trae todo 
        
        btn_editar.setEnabled(false);                           //esconder el boton editar
        
        DefaultTableModel modelo = new DefaultTableModel();     //establecer modelo de la tabla
        modelo.addColumn("Contador");
        modelo.addColumn("Id");
        modelo.addColumn("Nombres");
        modelo.addColumn("Apellidos");
        modelo.addColumn("N° Doc.");
        modelo.addColumn("Tipo Doc.");
        modelo.addColumn("Contacto");
        modelo.addColumn("Email");
        
        String[] datos = new String[8];                         //cargar los datos a la tabla
        try{
            
            ResultSet rs = baseDatos.buscarPor(parametro,valor);
            int i = 1;
            
            while(rs.next()){
                
                datos[0] = String.valueOf(i);                   //Contador
                datos[1] = rs.getString(1);                     //Id
                datos[2] = rs.getString(2);                     //Nombres
                datos[3] = rs.getString(3);                     //Apellidos
                datos[4] = rs.getString(4);                     //Documento
                datos[5] = rs.getString(5);                     //Tipo Documento
                datos[6] = rs.getString(6);                     //Contacto
                datos[7] = rs.getString(7);                     //Email
                      
                modelo.addRow(datos);
                i++;
            }
            
            table_alumno.setModel(modelo);
            TableColumnModel tcm = table_alumno.getColumnModel();
            tcm.removeColumn(tcm.getColumn(1));
            
        }catch(SQLException ex){
            System.out.println("Algo falló"+ex);
        }
    }
    
    void vaciarCampos(){                                 //Restaura los campos a sus valores predeterminados
        txtF_nombre.setText("");
        txtF_apellido.setText("");
        txtF_numDoc.setText("");
        txtF_contacto.setText("");
        txtF_email.setText("");
        comboB_tipoDoc.setSelectedIndex(0);
    }
   
    //Main--------------------------------------------------------------------------------------------------------------------------------------------
    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ABM_Alumnos().setVisible(true);
            }
        });
    }
    
    //Variables de clase propias----------------------------------------------------------------------------------------------------------------------
    
    public ConexionAlumnos baseDatos = new ConexionAlumnos();
    public Integer idEditar = 0;
    public Integer idCarrera = 0;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_editar;
    private javax.swing.JButton btn_guardar;
    private javax.swing.JComboBox<String> comboB_busqueda;
    private javax.swing.JComboBox<String> comboB_tipoDoc;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelD_apellido;
    private javax.swing.JLabel labelD_contacto;
    private javax.swing.JLabel labelD_email;
    private javax.swing.JLabel labelD_nombre;
    private javax.swing.JLabel labelD_numDoc;
    private javax.swing.JLabel labelD_tipoDoc;
    private javax.swing.JLabel labelT_buscarAlumno;
    private javax.swing.JLabel labelT_crearAlumno;
    private javax.swing.JMenuItem menuIt_borrar;
    private javax.swing.JMenuItem menuIt_editar;
    private javax.swing.JPopupMenu menu_opcionesTabla;
    private javax.swing.JPanel panel_botones;
    private javax.swing.JPanel panel_buscarAlumno;
    private javax.swing.JPanel panel_crearAlumno;
    private javax.swing.JPanel panel_tablaAlumno;
    private javax.swing.JSeparator separator1;
    private javax.swing.JSeparator separator2;
    private javax.swing.JTable table_alumno;
    private javax.swing.JTextField txtF_apellido;
    private javax.swing.JTextField txtF_busqueda;
    private javax.swing.JTextField txtF_contacto;
    private javax.swing.JTextField txtF_email;
    private javax.swing.JTextField txtF_nombre;
    private javax.swing.JTextField txtF_numDoc;
    // End of variables declaration//GEN-END:variables
}
