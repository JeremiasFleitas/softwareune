/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import clasesIniciales.Alumno;
import clasesIniciales.Carrera;
import clasesIniciales.Materia;
import com.mysql.jdbc.Connection;
import conexiones.Conexion;
import conexiones.ConexionAlumnos;
import conexiones.ConexionAlumnosMaterias;
import conexiones.ConexionAsistencias;
import conexiones.ConexionCarreras;
import conexiones.ConexionMaterias;
import conexiones.ConexionMateriasCarreras;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author Jeremias
 */
public class ABM_Asistencias extends javax.swing.JFrame {
    
    Conexion cc = new Conexion();
    Connection cn = cc.conexion();
    ArrayList mlistaAlumnos;
    ArrayList mlistaMaterias;
    ConexionAsistencias ca= new ConexionAsistencias();

    public ABM_Asistencias() {
        initComponents();
        cargarDatos();
        this.setLocationRelativeTo(null);
    }
    
    private void cargarDatos(){
        //mostrarDatosCarrera("","");
        //mostrarDatosMateria("","");
        mostrarDatosAsistencias("","");
        mostrarListaCarreras();
        //mostrarListaMaterias();
        //mostrarListaAlumnos();
    }
    
    private void mostrarListaCarreras(){
        comboB_selecCarrera.removeAllItems();
        listaCarreras = baseDatosCarrera.getListaCarreras();
        Iterator iterador = listaCarreras.iterator();
        while(iterador.hasNext()){
            Carrera carrera = (Carrera) iterador.next();
            comboB_selecCarrera.addItem(carrera);
        }
        
    }
       
    private void mostrarListaMaterias(int idCarrera){
        comboB_selecMateria.removeAllItems();
        listaMaterias = baseDatosMateriaPeriodo.getListaMateriasPeriodo(idCarrera);
        Iterator iterador = listaMaterias.iterator();
        while(iterador.hasNext()){
            Materia mater = (Materia) iterador.next();
            comboB_selecMateria.addItem(mater);
        }
        
    }
       
    private void mostrarListaAlumnos(int idMateriaPeriodo){
        comboB_selecAlumno.removeAllItems();
        listaAlumnos = baseDatosAlumnoMateria.getListaAlumnos(idMateriaPeriodo);
        Iterator iterador = listaAlumnos.iterator();
        //System.out.println("1");
        while(iterador.hasNext()){
            Alumno alum = (Alumno) iterador.next();
            //System.out.println("2");
            comboB_selecAlumno.addItem(alum);
        }
        
    }
                                       
    public static Date fechaActual(){
        Date fecha=new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date parsed = null;
        return(fecha);
    }
         
    public void mostrarDatosAsistencias(String parametro, String valor){
        btn_editar.setEnabled(false);                           //esconder el boton editar
        DefaultTableModel modelo = new DefaultTableModel(); 
        modelo.addColumn ("Contador");
        modelo.addColumn ("id");
        modelo.addColumn ("Fecha");
        modelo.addColumn ("Carrera");
        modelo.addColumn ("Materia");
        modelo.addColumn ("Alumno");
        modelo.addColumn ("Estado");
     
    String [] datos = new String [7];

    try{
            
            ResultSet rs = baseDatosAsistencia.buscarPor(parametro, valor);
            int i = 1;
            
            while(rs.next()){
                
                datos[0] = String.valueOf(i);                         //Contador
                datos[1] = rs.getString(1);                           //Fecha
                datos[2] = rs.getString(2);                           //Carrera
                datos[3] = rs.getString(3);                           //Materia
                datos[4] = rs.getString(4);                           //Alumno + Apellido Alumno
                datos[5] = rs.getString(5)+ " " + rs.getString(6);    //Estado 
                
                modelo.addRow(datos);
                i++;
            }
            
            table_asistencia.setModel(modelo);
           TableColumnModel tcm = table_asistencia.getColumnModel();
            
            tcm.removeColumn(tcm.getColumn(1)); //Id
            
        }catch(SQLException ex){
            System.out.println("Algo falló"+ex);
        }
    }
        
        
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        menu_opcionesTabla = new javax.swing.JPopupMenu();
        menuIt_editar = new javax.swing.JMenuItem();
        menuIt_borrar = new javax.swing.JMenuItem();
        panel_cargarAsistencia = new javax.swing.JPanel();
        labelT_cargarAsistencia = new javax.swing.JLabel();
        labelD_selecFecha = new javax.swing.JLabel();
        labelD_selecCarrera = new javax.swing.JLabel();
        labelD_selecMateria = new javax.swing.JLabel();
        labelD_selecEstado = new javax.swing.JLabel();
        labelD_selecAlumno = new javax.swing.JLabel();
        comboB_selecCarrera = new javax.swing.JComboBox<>();
        comboB_selecMateria = new javax.swing.JComboBox<>();
        comboB_selecAlumno = new javax.swing.JComboBox<>();
        dateC_selecFecha = new com.toedter.calendar.JDateChooser();
        separator1 = new javax.swing.JSeparator();
        radioB_presente = new javax.swing.JRadioButton();
        radioB_ausente = new javax.swing.JRadioButton();
        panel_botones = new javax.swing.JPanel();
        btn_guardar = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        btn_editar = new javax.swing.JButton();
        panel_tablaAsistencia = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_asistencia = new javax.swing.JTable();

        menuIt_editar.setText("jMenuItem1");

        menuIt_borrar.setText("jMenuItem2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        panel_cargarAsistencia.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        labelT_cargarAsistencia.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelT_cargarAsistencia.setText("Cargar asistencia");

        labelD_selecFecha.setText("Seleccionar fecha:");

        labelD_selecCarrera.setText("Seleccionar carrera:");

        labelD_selecMateria.setText("Seleccionar materia:");

        labelD_selecEstado.setText("Seleccionar estado:");

        labelD_selecAlumno.setText("Seleccionar alumno:");

        comboB_selecCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboB_selecCarreraActionPerformed(evt);
            }
        });

        comboB_selecMateria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboB_selecMateriaActionPerformed(evt);
            }
        });

        buttonGroup1.add(radioB_presente);
        radioB_presente.setText("Presente");

        buttonGroup1.add(radioB_ausente);
        radioB_ausente.setText("Ausente");

        javax.swing.GroupLayout panel_cargarAsistenciaLayout = new javax.swing.GroupLayout(panel_cargarAsistencia);
        panel_cargarAsistencia.setLayout(panel_cargarAsistenciaLayout);
        panel_cargarAsistenciaLayout.setHorizontalGroup(
            panel_cargarAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_cargarAsistenciaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_cargarAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_cargarAsistenciaLayout.createSequentialGroup()
                        .addComponent(labelD_selecFecha)
                        .addGap(30, 30, 30)
                        .addComponent(dateC_selecFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_cargarAsistenciaLayout.createSequentialGroup()
                        .addComponent(labelD_selecEstado)
                        .addGap(24, 24, 24)
                        .addComponent(radioB_presente)
                        .addGap(11, 11, 11)
                        .addComponent(radioB_ausente))
                    .addGroup(panel_cargarAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(panel_cargarAsistenciaLayout.createSequentialGroup()
                            .addComponent(labelD_selecAlumno)
                            .addGap(23, 23, 23)
                            .addComponent(comboB_selecAlumno, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(panel_cargarAsistenciaLayout.createSequentialGroup()
                            .addComponent(labelD_selecMateria)
                            .addGap(21, 21, 21)
                            .addComponent(comboB_selecMateria, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(panel_cargarAsistenciaLayout.createSequentialGroup()
                            .addComponent(labelD_selecCarrera)
                            .addGap(22, 22, 22)
                            .addComponent(comboB_selecCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, 408, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panel_cargarAsistenciaLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(labelT_cargarAsistencia)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(separator1, javax.swing.GroupLayout.PREFERRED_SIZE, 441, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panel_cargarAsistenciaLayout.setVerticalGroup(
            panel_cargarAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_cargarAsistenciaLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(panel_cargarAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelT_cargarAsistencia)
                    .addComponent(separator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panel_cargarAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelD_selecFecha)
                    .addComponent(dateC_selecFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(panel_cargarAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboB_selecCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelD_selecCarrera))
                .addGap(20, 20, 20)
                .addGroup(panel_cargarAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboB_selecMateria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelD_selecMateria))
                .addGap(20, 20, 20)
                .addGroup(panel_cargarAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboB_selecAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelD_selecAlumno))
                .addGap(20, 20, 20)
                .addGroup(panel_cargarAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_cargarAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(radioB_presente)
                        .addComponent(labelD_selecEstado))
                    .addComponent(radioB_ausente))
                .addContainerGap(11, Short.MAX_VALUE))
        );

        panel_botones.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btn_guardar.setText("Guardar");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });

        btn_cancelar.setText("Cancelar");

        btn_editar.setText("Editar");
        btn_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_botonesLayout = new javax.swing.GroupLayout(panel_botones);
        panel_botones.setLayout(panel_botonesLayout);
        panel_botonesLayout.setHorizontalGroup(
            panel_botonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_botonesLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(panel_botonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_guardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_editar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_cancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(30, Short.MAX_VALUE))
        );
        panel_botonesLayout.setVerticalGroup(
            panel_botonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_botonesLayout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addComponent(btn_guardar)
                .addGap(18, 18, 18)
                .addComponent(btn_editar)
                .addGap(15, 15, 15)
                .addComponent(btn_cancelar)
                .addContainerGap(56, Short.MAX_VALUE))
        );

        panel_tablaAsistencia.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        table_asistencia.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        table_asistencia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Contador", "Fecha", "Carrera", "Materia", "Alumno", "Estado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(table_asistencia);

        javax.swing.GroupLayout panel_tablaAsistenciaLayout = new javax.swing.GroupLayout(panel_tablaAsistencia);
        panel_tablaAsistencia.setLayout(panel_tablaAsistenciaLayout);
        panel_tablaAsistenciaLayout.setHorizontalGroup(
            panel_tablaAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_tablaAsistenciaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        panel_tablaAsistenciaLayout.setVerticalGroup(
            panel_tablaAsistenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_tablaAsistenciaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel_tablaAsistencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panel_cargarAsistencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panel_botones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panel_botones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel_cargarAsistencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel_tablaAsistencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed
        if(comboB_selecCarrera.getSelectedItem().toString().equals("")){
           JOptionPane.showMessageDialog(rootPane, "¡No puedes guardar un valor vacio!");
        }else{
        java.sql.Date data = null;
        data = new java.sql.Date(dateC_selecFecha.getDate().getTime());
            Carrera carr  = new Carrera();
            carr =  (Carrera)comboB_selecCarrera.getSelectedItem();
            Materia mat = new Materia();
            mat = (Materia)comboB_selecMateria.getSelectedItem();
            Alumno alu = new Alumno();
            String Asistencias;
            if(radioB_presente.isSelected()){
            Asistencias = "Presente";
          }else{
            Asistencias = "Ausente";
        }
            alu = (Alumno)comboB_selecAlumno.getSelectedItem();
           baseDatosAsistencia.guardar(data, carr.getId(), mat.getId(), alu.getId_alumno(), Asistencias);
            JOptionPane.showMessageDialog(rootPane, "¡Guardado con Éxito!");
            
            //vaciarMateria();
            cargarDatos();
        }        

          
    }//GEN-LAST:event_btn_guardarActionPerformed

    private void btn_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarActionPerformed
        /**/if(comboB_selecCarrera.getSelectedItem().toString().equals("")){
           JOptionPane.showMessageDialog(rootPane, "¡No puedes guardar un valor vacio!");
        }else{
        java.sql.Date data = null;
        data = new java.sql.Date(dateC_selecFecha.getDate().getTime());
            Carrera carr  = new Carrera();
            carr =  (Carrera)comboB_selecCarrera.getSelectedItem();
            Materia mat = new Materia();
            mat = (Materia)comboB_selecMateria.getSelectedItem();
            Alumno alu = new Alumno();
            String Asistencias;
            if(radioB_presente.isSelected()){
            Asistencias = "Presente";
          }else{
            Asistencias = "Ausente";
        }
            alu = (Alumno)comboB_selecAlumno.getSelectedItem();
           baseDatosAsistencia.actualizar(idEditar,data, carr.getId(), mat.getId(), alu.getId_alumno(),Asistencias);
            JOptionPane.showMessageDialog(rootPane, "¡Editado con Éxito!");
            
        //vaciarMateria();
        cargarDatos();
        mostrarDatosAsistencias("","");
<<<<<<< HEAD
        btn_editar.setVisible(true);
        btn_guardar.setVisible(false);
        }
=======
        
        JOptionPane.showMessageDialog(rootPane, "¡Editado con Éxito!");
        
        btn_editar.setEnabled(false);
        btn_guardar.setEnabled(true);
>>>>>>> 8644212d993503e02b008b08d9e5ab6ced9e0c53
    }//GEN-LAST:event_btn_editarActionPerformed

    private void comboB_selecCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboB_selecCarreraActionPerformed
        Carrera cS = (Carrera) comboB_selecCarrera.getSelectedItem();
        mostrarListaMaterias(cS.getId());
        //comboB_selecAlumno.removeAllItems();
    }//GEN-LAST:event_comboB_selecCarreraActionPerformed

    private void comboB_selecMateriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboB_selecMateriaActionPerformed
        if (comboB_selecMateria.getItemCount()!=0){
            Materia ma=(Materia) comboB_selecMateria.getSelectedItem();
            System.out.println(ma.getId());
            mostrarListaAlumnos(ma.getId());
        }
    }//GEN-LAST:event_comboB_selecMateriaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ABM_Asistencias().setVisible(true);
            }
        });
    }
    //Variables Propias
    private ConexionCarreras baseDatosCarrera = new ConexionCarreras();
    private ConexionMaterias baseDatosMateria = new ConexionMaterias();
    private ConexionMateriasCarreras baseDatosMateriaPeriodo = new ConexionMateriasCarreras();
    private ConexionAlumnos baseDatosAlumno = new ConexionAlumnos();
    private ConexionAlumnosMaterias baseDatosAlumnoMateria = new ConexionAlumnosMaterias();
    public ConexionAsistencias baseDatosAsistencia = new ConexionAsistencias();
    private ArrayList listaCarreras;
    private ArrayList<Materia> listaMaterias;
    private ArrayList listaAlumnos;
    private Integer idEditar= 0;

   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_editar;
    private javax.swing.JButton btn_guardar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<Alumno> comboB_selecAlumno;
    private javax.swing.JComboBox<Carrera> comboB_selecCarrera;
    private javax.swing.JComboBox<Materia> comboB_selecMateria;
    private com.toedter.calendar.JDateChooser dateC_selecFecha;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelD_selecAlumno;
    private javax.swing.JLabel labelD_selecCarrera;
    private javax.swing.JLabel labelD_selecEstado;
    private javax.swing.JLabel labelD_selecFecha;
    private javax.swing.JLabel labelD_selecMateria;
    private javax.swing.JLabel labelT_cargarAsistencia;
    private javax.swing.JMenuItem menuIt_borrar;
    private javax.swing.JMenuItem menuIt_editar;
    private javax.swing.JPopupMenu menu_opcionesTabla;
    private javax.swing.JPanel panel_botones;
    private javax.swing.JPanel panel_cargarAsistencia;
    private javax.swing.JPanel panel_tablaAsistencia;
    private javax.swing.JRadioButton radioB_ausente;
    private javax.swing.JRadioButton radioB_presente;
    private javax.swing.JSeparator separator1;
    private javax.swing.JTable table_asistencia;
    // End of variables declaration//GEN-END:variables

}
