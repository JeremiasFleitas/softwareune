package interfaces;

import conexiones.ConexionDocentes;
import javax.swing.table.DefaultTableModel;
import com.mysql.jdbc.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.table.TableColumnModel;
import sistema.ABM_CV_Docentes;

/**
 *
 * @author Jeremias
 */
public class ABM_Docentes extends javax.swing.JFrame {

//Constructor---------------------------------------------------------------------------------------------    
    public ABM_Docentes() {
        initComponents();
        mostrarDatos("","");
        this.setLocationRelativeTo(null);
    }
    
   //Código Generado------------------------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menu_opcionesTabla = new javax.swing.JPopupMenu();
        menuIt_editar = new javax.swing.JMenuItem();
        menuIt_borrar = new javax.swing.JMenuItem();
        panel_crearDocente = new javax.swing.JPanel();
        nombre_label = new javax.swing.JLabel();
        txtF_nombre = new javax.swing.JTextField();
        contacto_label = new javax.swing.JLabel();
        txtF_email = new javax.swing.JTextField();
        apellido_label = new javax.swing.JLabel();
        contacto_label1 = new javax.swing.JLabel();
        txtF_apellido = new javax.swing.JTextField();
        txtF_contacto = new javax.swing.JTextField();
        tituloCrear_label = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        tipoDoc_label = new javax.swing.JLabel();
        comboB_tipoDoc = new javax.swing.JComboBox<>();
        cedula_label = new javax.swing.JLabel();
        txtF_numDoc = new javax.swing.JTextField();
        panel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_docente = new javax.swing.JTable();
        panel2 = new javax.swing.JPanel();
        btn_guardar = new javax.swing.JButton();
        btn_editar = new javax.swing.JButton();
        btn_CV = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        panel_buscarDocente = new javax.swing.JPanel();
        btn_buscar = new javax.swing.JButton();
        comboB_busqueda = new javax.swing.JComboBox<>();
        txtF_busqueda = new javax.swing.JTextField();
        tituloBuscar_label = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();

        menuIt_editar.setText("Editar");
        menuIt_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_editarActionPerformed(evt);
            }
        });
        menu_opcionesTabla.add(menuIt_editar);

        menuIt_borrar.setText("Borrar");
        menuIt_borrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_borrarActionPerformed(evt);
            }
        });
        menu_opcionesTabla.add(menuIt_borrar);

        menu_opcionesTabla.getAccessibleContext().setAccessibleParent(menu_opcionesTabla);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        panel_crearDocente.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        panel_crearDocente.setPreferredSize(new java.awt.Dimension(582, 168));

        nombre_label.setText("* Nombres:");

        contacto_label.setText("Contacto:");

        apellido_label.setText("* Apellidos:");

        contacto_label1.setText("E-mail:");

        tituloCrear_label.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tituloCrear_label.setText("Crear Docente");

        tipoDoc_label.setText("Tipo doc.");

        comboB_tipoDoc.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "N/E", "C.I.N.", "D.N.I.", "Pasaporte" }));

        cedula_label.setText("* N° Doc.:");

        javax.swing.GroupLayout panel_crearDocenteLayout = new javax.swing.GroupLayout(panel_crearDocente);
        panel_crearDocente.setLayout(panel_crearDocenteLayout);
        panel_crearDocenteLayout.setHorizontalGroup(
            panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_crearDocenteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panel_crearDocenteLayout.createSequentialGroup()
                        .addComponent(tituloCrear_label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panel_crearDocenteLayout.createSequentialGroup()
                        .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(contacto_label)
                            .addComponent(nombre_label)
                            .addComponent(tipoDoc_label))
                        .addGap(18, 18, 18)
                        .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(comboB_tipoDoc, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtF_nombre, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)
                            .addComponent(txtF_contacto))
                        .addGap(27, 27, 27)
                        .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cedula_label, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(apellido_label)
                            .addGroup(panel_crearDocenteLayout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(contacto_label1)))
                        .addGap(18, 18, 18)
                        .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtF_numDoc, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                            .addComponent(txtF_apellido, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtF_email))))
                .addContainerGap(17, Short.MAX_VALUE))
        );
        panel_crearDocenteLayout.setVerticalGroup(
            panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_crearDocenteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tituloCrear_label)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_crearDocenteLayout.createSequentialGroup()
                        .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nombre_label)
                            .addComponent(txtF_nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tipoDoc_label)
                            .addComponent(comboB_tipoDoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(contacto_label)
                            .addComponent(txtF_contacto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panel_crearDocenteLayout.createSequentialGroup()
                        .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(apellido_label)
                            .addComponent(txtF_apellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtF_numDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cedula_label))
                        .addGap(18, 18, 18)
                        .addGroup(panel_crearDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtF_email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(contacto_label1))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        table_docente.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        table_docente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Contador", "Id", "Nombres", "Apellidos", "N° Doc.", "Tipo Doc.", "Contacto", "Email"
            }
        ));
        table_docente.setComponentPopupMenu(menu_opcionesTabla);
        table_docente.setShowHorizontalLines(false);
        jScrollPane2.setViewportView(table_docente);

        javax.swing.GroupLayout panel4Layout = new javax.swing.GroupLayout(panel4);
        panel4.setLayout(panel4Layout);
        panel4Layout.setHorizontalGroup(
            panel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        panel4Layout.setVerticalGroup(
            panel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                .addContainerGap())
        );

        panel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        panel2.setPreferredSize(new java.awt.Dimension(115, 113));

        btn_guardar.setText("Guardar");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });

        btn_editar.setText("Editar");
        btn_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarActionPerformed(evt);
            }
        });

        btn_CV.setText("CV");
        btn_CV.setToolTipText("");
        btn_CV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CVActionPerformed(evt);
            }
        });

        btn_cancelar.setText("Cancelar");
        btn_cancelar.setToolTipText("");
        btn_cancelar.setMaximumSize(new java.awt.Dimension(61, 23));
        btn_cancelar.setMinimumSize(new java.awt.Dimension(61, 23));
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_guardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_editar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_CV, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23))
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(btn_guardar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_editar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_CV)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        panel_buscarDocente.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        comboB_busqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mostrar Todo", "Nombre", "Apellido", "Documento", "Contacto", "Email" }));
        comboB_busqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboB_busquedaActionPerformed(evt);
            }
        });

        txtF_busqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtF_busquedaActionPerformed(evt);
            }
        });

        tituloBuscar_label.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tituloBuscar_label.setText("Buscar Docente");

        javax.swing.GroupLayout panel_buscarDocenteLayout = new javax.swing.GroupLayout(panel_buscarDocente);
        panel_buscarDocente.setLayout(panel_buscarDocenteLayout);
        panel_buscarDocenteLayout.setHorizontalGroup(
            panel_buscarDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_buscarDocenteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_buscarDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_buscarDocenteLayout.createSequentialGroup()
                        .addComponent(comboB_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtF_busqueda)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_buscar)
                        .addGap(34, 34, 34))
                    .addGroup(panel_buscarDocenteLayout.createSequentialGroup()
                        .addComponent(tituloBuscar_label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator3))))
        );
        panel_buscarDocenteLayout.setVerticalGroup(
            panel_buscarDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_buscarDocenteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_buscarDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tituloBuscar_label)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_buscarDocenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_buscar)
                    .addComponent(comboB_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtF_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panel_crearDocente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panel2, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE))
                    .addComponent(panel_buscarDocente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panel_crearDocente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel2, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel_buscarDocente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Eventos-------------------------------------------------------------------------------------------------
    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed
        if((txtF_nombre.getText().equals(""))&&(txtF_apellido.getText().equals(""))&&(txtF_numDoc.getText().equals(""))){
           JOptionPane.showMessageDialog(rootPane, "¡Debes completar todos los campos obligatorios(*)!");
        }else{
            baseDatos.guardar(txtF_nombre.getText(), txtF_apellido.getText(),txtF_numDoc.getText(),
                    comboB_tipoDoc.getSelectedItem().toString(),txtF_contacto.getText(),txtF_email.getText());
            JOptionPane.showMessageDialog(rootPane, "¡Guardado con Éxito!");
            vaciarCampos();
            mostrarDatos("","");
        }
    }//GEN-LAST:event_btn_guardarActionPerformed

    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        String valor = txtF_busqueda.getText();
        String parametro = comboB_busqueda.getSelectedItem().toString();
        mostrarDatos(parametro,valor);
    }//GEN-LAST:event_btn_buscarActionPerformed

    private void btn_CVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CVActionPerformed
        ABM_CV_Docentes CVDocentes = new ABM_CV_Docentes();
        CVDocentes.setVisible(true);
    }//GEN-LAST:event_btn_CVActionPerformed

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        if(btn_editar.isEnabled()){
            vaciarCampos();
            btn_guardar.setEnabled(true);
            btn_editar.setEnabled(false);
        }else{
            vaciarCampos();
        }
    }//GEN-LAST:event_btn_cancelarActionPerformed

    private void txtF_busquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtF_busquedaActionPerformed
        String valor = txtF_busqueda.getText();
        String parametro = comboB_busqueda.getSelectedItem().toString();
        mostrarDatos(parametro,valor);
    }//GEN-LAST:event_txtF_busquedaActionPerformed

    private void btn_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarActionPerformed
        baseDatos.actualizar(idEditar, txtF_nombre.getText(), txtF_apellido.getText(),txtF_numDoc.getText(),
            comboB_tipoDoc.getSelectedItem().toString(),txtF_contacto.getText(),txtF_email.getText());

        vaciarCampos();
        mostrarDatos("","");

        JOptionPane.showMessageDialog(rootPane, "¡Editado con Éxito!");

        btn_editar.setEnabled(false);
        btn_guardar.setEnabled(true);
    }//GEN-LAST:event_btn_editarActionPerformed

    private void menuIt_borrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_borrarActionPerformed
        int resp = JOptionPane.showConfirmDialog(null, "¿Está seguro?", "¡Alerta!", JOptionPane.YES_NO_OPTION);
        
        if(resp==0){
            int filaSeleccionada = table_docente.getSelectedRow();
            idEditar = Integer.parseInt(table_docente.getModel().getValueAt(filaSeleccionada, 1).toString());
            baseDatos.delete(idEditar);
            JOptionPane.showMessageDialog(rootPane, "¡Borrado con Éxito!");
            mostrarDatos("","");
            idEditar = null;
        } 
    }//GEN-LAST:event_menuIt_borrarActionPerformed

    private void menuIt_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_editarActionPerformed
        int filaSeleccionada = table_docente.getSelectedRow();
        
        idEditar = Integer.parseInt(table_docente.getModel().getValueAt(filaSeleccionada, 1).toString());
 
        txtF_nombre.setText(table_docente.getValueAt(filaSeleccionada, 1).toString().trim());
        txtF_apellido.setText(table_docente.getValueAt(filaSeleccionada, 2).toString().trim());
        txtF_numDoc.setText(table_docente.getValueAt(filaSeleccionada, 3).toString().trim());
        
        String tipo_doc = table_docente.getValueAt(filaSeleccionada, 4).toString().trim(); 
        
        if (tipo_doc.equals("N/E")){
            comboB_tipoDoc.setSelectedIndex(0);
        }else if(tipo_doc.equals("C.I.N.")){
            comboB_tipoDoc.setSelectedIndex(1);
        }else if(tipo_doc.equals("D.N.I.")){
            comboB_tipoDoc.setSelectedIndex(2);
        }else{
            comboB_tipoDoc.setSelectedIndex(3);
        }
        
        txtF_contacto.setText(table_docente.getValueAt(filaSeleccionada, 5).toString().trim());
        txtF_email.setText(table_docente.getValueAt(filaSeleccionada, 6).toString().trim());
       
        btn_editar.setEnabled(true);
        btn_guardar.setEnabled(false);
    }//GEN-LAST:event_menuIt_editarActionPerformed

    private void comboB_busquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboB_busquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboB_busquedaActionPerformed

    // Own functions ***********************************************************
    void mostrarDatos(String parametro, String valor){     //trae los datos de acuerdo al parametro y al valor, mostrarDatos("","") trae todo 
        
        btn_editar.setEnabled(false);                           //esconder el boton editar
        
        DefaultTableModel modelo = new DefaultTableModel();     //establecer modelo de la tabla
        modelo.addColumn("Contador");
        modelo.addColumn("Id");
        modelo.addColumn("Nombres");
        modelo.addColumn("Apellidos");
        modelo.addColumn("N° Doc.");
        modelo.addColumn("Tipo Doc.");
        modelo.addColumn("Contacto");
        modelo.addColumn("Email");
        
        String[] datos = new String[8];                         //cargar los datos a la tabla
        try{
            
            ResultSet rs = baseDatos.buscarPor(parametro,valor);
            int i = 1;
            
            while(rs.next()){
                
                datos[0] = String.valueOf(i);                   //Contador
                datos[1] = rs.getString(1);                     //Id
                datos[2] = rs.getString(2);                     //Nombres
                datos[3] = rs.getString(3);                     //Apellidos
                datos[4] = rs.getString(4);                     //Documento
                datos[5] = rs.getString(5);                     //Tipo Documento
                datos[6] = rs.getString(6);                     //Contacto
                datos[7] = rs.getString(7);                     //Email
                      
                modelo.addRow(datos);
                i++;
            }
            
            table_docente.setModel(modelo);
            TableColumnModel tcm = table_docente.getColumnModel();
            tcm.removeColumn(tcm.getColumn(1));
            
        }catch(SQLException ex){
            System.out.println("Algo falló"+ex);
        }
    }
        
    
    void vaciarCampos(){                                 //Restaura los campos a sus valores predeterminados
        txtF_nombre.setText("");
        txtF_apellido.setText("");
        txtF_numDoc.setText("");
        txtF_contacto.setText("");
        txtF_email.setText("");
        comboB_tipoDoc.setSelectedIndex(0);
    }
    
      //Main--------------------------------------------------------------------------------------------------------------------------------------------
    public static void main(String args[]) { 
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ABM_Docentes().setVisible(true);
            }
        });
    }
    
    //Variables de clase propia
    
    public ConexionDocentes baseDatos = new ConexionDocentes();
    public Integer idEditar = 0;
    public Integer idCarrera = 0;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel apellido_label;
    private javax.swing.JButton btn_CV;
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_editar;
    private javax.swing.JButton btn_guardar;
    private javax.swing.JLabel cedula_label;
    private javax.swing.JComboBox<String> comboB_busqueda;
    private javax.swing.JComboBox<String> comboB_tipoDoc;
    private javax.swing.JLabel contacto_label;
    private javax.swing.JLabel contacto_label1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JMenuItem menuIt_borrar;
    private javax.swing.JMenuItem menuIt_editar;
    private javax.swing.JPopupMenu menu_opcionesTabla;
    private javax.swing.JLabel nombre_label;
    private javax.swing.JPanel panel2;
    private javax.swing.JPanel panel4;
    private javax.swing.JPanel panel_buscarDocente;
    private javax.swing.JPanel panel_crearDocente;
    private javax.swing.JTable table_docente;
    private javax.swing.JLabel tipoDoc_label;
    private javax.swing.JLabel tituloBuscar_label;
    private javax.swing.JLabel tituloCrear_label;
    private javax.swing.JTextField txtF_apellido;
    private javax.swing.JTextField txtF_busqueda;
    private javax.swing.JTextField txtF_contacto;
    private javax.swing.JTextField txtF_email;
    private javax.swing.JTextField txtF_nombre;
    private javax.swing.JTextField txtF_numDoc;
    // End of variables declaration//GEN-END:variables
}
