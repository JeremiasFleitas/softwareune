/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import clasesIniciales.Carrera;
import clasesIniciales.Docente;
import clasesIniciales.Materia;
import conexiones.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;


/**
 *
 * @author Fernando
 */
public class ABM_Carreras_Materias extends javax.swing.JFrame {

    //Constructor
    public ABM_Carreras_Materias() {
        initComponents();
        cargarDatos();
        this.setLocationRelativeTo(null);
    }

    //Init Components
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menu_carrera = new javax.swing.JPopupMenu();
        menuIt_editarCarrera = new javax.swing.JMenuItem();
        menuIt_borrarCarrera = new javax.swing.JMenuItem();
        menu_materia = new javax.swing.JPopupMenu();
        menuIt_editarMateria = new javax.swing.JMenuItem();
        menuIt_borrarMateria = new javax.swing.JMenuItem();
        menu_materiaCarrera = new javax.swing.JPopupMenu();
        menuIt_editarMateriaCarrera = new javax.swing.JMenuItem();
        menuIt_borrarMateriaCarrera = new javax.swing.JMenuItem();
        panel_contenedor = new javax.swing.JPanel();
        labelT_materiaCarrera = new javax.swing.JLabel();
        labelD_carreraMateria = new javax.swing.JLabel();
        labelD_materiaCarrera = new javax.swing.JLabel();
        labelD_fechaInicio = new javax.swing.JLabel();
        labelD_docenteMateria = new javax.swing.JLabel();
        comboB_carrera = new javax.swing.JComboBox<>();
        comboB_materia = new javax.swing.JComboBox<>();
        comboB_docente = new javax.swing.JComboBox<>();
        btn_editarMateriaCarrera = new javax.swing.JButton();
        labelD_fechaFin = new javax.swing.JLabel();
        btn_guardarMateriaCarrera = new javax.swing.JButton();
        btn_cancelarMateriaCarrera = new javax.swing.JButton();
        labelD_hsSemana = new javax.swing.JLabel();
        txtF_hsSemana = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        table_materiaCarrera = new javax.swing.JTable();
        btn_buscarMateriaCarrera = new javax.swing.JButton();
        txtF_busqueda = new javax.swing.JTextField();
        comboB_busqueda = new javax.swing.JComboBox<>();
        dateC_fechaInicio = new com.toedter.calendar.JDateChooser();
        dateC_fechaFin = new com.toedter.calendar.JDateChooser();
        jPanel1 = new javax.swing.JPanel();
        labelT_carrera = new javax.swing.JLabel();
        labelD_nombreCarrera = new javax.swing.JLabel();
        txtF_nombreCarrera = new javax.swing.JTextField();
        labelD_decripcionCarrera = new javax.swing.JLabel();
        txtF_descripcionCarrera = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_carrera = new javax.swing.JTable();
        btn_guardarCarrera = new javax.swing.JButton();
        btn_editarCarrera = new javax.swing.JButton();
        btn_cancelarCarrera = new javax.swing.JButton();
        btn_buscarCarrera = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        labelT_materia = new javax.swing.JLabel();
        labelD_nombreMateria = new javax.swing.JLabel();
        txtF_nombreMateria = new javax.swing.JTextField();
        btn_guardarMateria = new javax.swing.JButton();
        btn_editarMateria = new javax.swing.JButton();
        txtF_descripcionMateria = new javax.swing.JTextField();
        labelD_descripcionMateria = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        table_materia = new javax.swing.JTable();
        btn_buscarMateria = new javax.swing.JButton();
        btn_cancelarMateria = new javax.swing.JButton();

        menuIt_editarCarrera.setText("Editar");
        menuIt_editarCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_editarCarreraActionPerformed(evt);
            }
        });
        menu_carrera.add(menuIt_editarCarrera);

        menuIt_borrarCarrera.setText("Borrar");
        menuIt_borrarCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_borrarCarreraActionPerformed(evt);
            }
        });
        menu_carrera.add(menuIt_borrarCarrera);

        menuIt_editarMateria.setText("Editar");
        menuIt_editarMateria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_editarMateriaActionPerformed(evt);
            }
        });
        menu_materia.add(menuIt_editarMateria);

        menuIt_borrarMateria.setText("Borrar");
        menuIt_borrarMateria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_borrarMateriaActionPerformed(evt);
            }
        });
        menu_materia.add(menuIt_borrarMateria);

        menuIt_editarMateriaCarrera.setText("Editar");
        menuIt_editarMateriaCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_editarMateriaCarreraActionPerformed(evt);
            }
        });
        menu_materiaCarrera.add(menuIt_editarMateriaCarrera);

        menuIt_borrarMateriaCarrera.setText("Borrar");
        menuIt_borrarMateriaCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuIt_borrarMateriaCarreraActionPerformed(evt);
            }
        });
        menu_materiaCarrera.add(menuIt_borrarMateriaCarrera);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panel_contenedor.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        labelT_materiaCarrera.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelT_materiaCarrera.setText("Vincular Materia a Carrera");

        labelD_carreraMateria.setText("Carrera:");

        labelD_materiaCarrera.setText("Materia:");

        labelD_fechaInicio.setText("Fecha Inicio:");

        labelD_docenteMateria.setText("Docente:");

        btn_editarMateriaCarrera.setText("Editar");
        btn_editarMateriaCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarMateriaCarreraActionPerformed(evt);
            }
        });

        labelD_fechaFin.setText("Fecha Fin:");

        btn_guardarMateriaCarrera.setText("Guardar");
        btn_guardarMateriaCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarMateriaCarreraActionPerformed(evt);
            }
        });

        btn_cancelarMateriaCarrera.setText("Cancelar");
        btn_cancelarMateriaCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarMateriaCarreraActionPerformed(evt);
            }
        });

        labelD_hsSemana.setText("Hs Cátedra/S:");

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        table_materiaCarrera.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Contador", "Id", "Materia", "Docente", "Carrera", "Hs Semana", "Fecha Inicio", "Fecha Fin"
            }
        ));
        table_materiaCarrera.setComponentPopupMenu(menu_materiaCarrera);
        jScrollPane4.setViewportView(table_materiaCarrera);

        btn_buscarMateriaCarrera.setText("Buscar");
        btn_buscarMateriaCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarMateriaCarreraActionPerformed(evt);
            }
        });

        txtF_busqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtF_busquedaActionPerformed(evt);
            }
        });

        comboB_busqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mostrar Todo", "Materia", "Docente", "Carrera" }));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(comboB_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtF_busqueda)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_buscarMateriaCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_buscarMateriaCarrera)
                    .addComponent(txtF_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboB_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout panel_contenedorLayout = new javax.swing.GroupLayout(panel_contenedor);
        panel_contenedor.setLayout(panel_contenedorLayout);
        panel_contenedorLayout.setHorizontalGroup(
            panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_contenedorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_contenedorLayout.createSequentialGroup()
                        .addComponent(labelT_materiaCarrera)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(panel_contenedorLayout.createSequentialGroup()
                        .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(panel_contenedorLayout.createSequentialGroup()
                                .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelD_hsSemana)
                                    .addComponent(labelD_fechaInicio)
                                    .addComponent(labelD_fechaFin)
                                    .addComponent(labelD_carreraMateria)
                                    .addComponent(labelD_materiaCarrera)
                                    .addComponent(labelD_docenteMateria))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(comboB_carrera, 0, 254, Short.MAX_VALUE)
                                    .addComponent(comboB_materia, 0, 254, Short.MAX_VALUE)
                                    .addComponent(comboB_docente, 0, 254, Short.MAX_VALUE)
                                    .addComponent(txtF_hsSemana)
                                    .addComponent(dateC_fechaInicio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(dateC_fechaFin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(panel_contenedorLayout.createSequentialGroup()
                                .addComponent(btn_guardarMateriaCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                                .addComponent(btn_editarMateriaCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btn_cancelarMateriaCarrera, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)))
                        .addGap(23, 23, 23)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        panel_contenedorLayout.setVerticalGroup(
            panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_contenedorLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(labelT_materiaCarrera)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panel_contenedorLayout.createSequentialGroup()
                        .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panel_contenedorLayout.createSequentialGroup()
                                .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(labelD_carreraMateria)
                                    .addComponent(comboB_carrera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(comboB_materia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(labelD_materiaCarrera))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(comboB_docente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(labelD_docenteMateria))
                                .addGap(18, 18, 18)
                                .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(labelD_hsSemana)
                                    .addComponent(txtF_hsSemana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(20, 20, 20)
                                .addComponent(labelD_fechaInicio))
                            .addComponent(dateC_fechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(14, 14, 14)
                        .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelD_fechaFin)
                            .addComponent(dateC_fechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panel_contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btn_cancelarMateriaCarrera)
                                .addComponent(btn_editarMateriaCarrera))
                            .addComponent(btn_guardarMateriaCarrera, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        labelT_carrera.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelT_carrera.setText("Carreras");

        labelD_nombreCarrera.setText("Nombre:");

        labelD_decripcionCarrera.setText("Descripción:");

        table_carrera.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nombre", "Descripción"
            }
        ));
        table_carrera.setComponentPopupMenu(menu_carrera);
        jScrollPane2.setViewportView(table_carrera);

        btn_guardarCarrera.setText("Guardar");
        btn_guardarCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarCarreraActionPerformed(evt);
            }
        });

        btn_editarCarrera.setText("Editar");
        btn_editarCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarCarreraActionPerformed(evt);
            }
        });

        btn_cancelarCarrera.setText("Cancelar");
        btn_cancelarCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarCarreraActionPerformed(evt);
            }
        });

        btn_buscarCarrera.setText("Buscar");
        btn_buscarCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarCarreraActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(labelT_carrera, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(371, 371, 371))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelD_nombreCarrera)
                                    .addComponent(labelD_decripcionCarrera))
                                .addGap(5, 5, 5)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtF_nombreCarrera, javax.swing.GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE)
                                    .addComponent(txtF_descripcionCarrera))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btn_buscarCarrera, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btn_editarCarrera, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btn_guardarCarrera, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btn_cancelarCarrera, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(labelT_carrera)
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelD_nombreCarrera, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtF_nombreCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_guardarCarrera)
                        .addComponent(btn_buscarCarrera)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelD_decripcionCarrera)
                    .addComponent(txtF_descripcionCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cancelarCarrera)
                    .addComponent(btn_editarCarrera))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        labelT_materia.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelT_materia.setText("Materias");

        labelD_nombreMateria.setText("Nombre:");

        btn_guardarMateria.setText("Guardar");
        btn_guardarMateria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarMateriaActionPerformed(evt);
            }
        });

        btn_editarMateria.setText("Editar");
        btn_editarMateria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarMateriaActionPerformed(evt);
            }
        });

        labelD_descripcionMateria.setText("Descripción:");

        table_materia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nombre", "Descripción"
            }
        ));
        table_materia.setComponentPopupMenu(menu_materia);
        jScrollPane3.setViewportView(table_materia);

        btn_buscarMateria.setText("Buscar");
        btn_buscarMateria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarMateriaActionPerformed(evt);
            }
        });

        btn_cancelarMateria.setText("Cancelar");
        btn_cancelarMateria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarMateriaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelD_descripcionMateria)
                            .addComponent(labelD_nombreMateria))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtF_nombreMateria, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                            .addComponent(txtF_descripcionMateria))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn_editarMateria, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_buscarMateria, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btn_guardarMateria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_cancelarMateria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(labelT_materia)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(labelT_materia)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(labelD_nombreMateria))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_buscarMateria)
                        .addComponent(btn_guardarMateria)
                        .addComponent(txtF_nombreMateria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_editarMateria)
                            .addComponent(btn_cancelarMateria)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelD_descripcionMateria)
                            .addComponent(txtF_descripcionMateria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(11, 11, 11)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel_contenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel_contenedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    //Eventos
    
    private void btn_guardarCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarCarreraActionPerformed
        if(txtF_nombreCarrera.getText().equals("")){
           JOptionPane.showMessageDialog(rootPane, "¡Debes introducir un nombre!");
        }else{
            baseDatosCarrera.guardar(txtF_nombreCarrera.getText(), txtF_descripcionCarrera.getText());
            JOptionPane.showMessageDialog(rootPane, "¡Guardado con Éxito!");
            vaciarCarrera();
            cargarDatos();
        }
    }//GEN-LAST:event_btn_guardarCarreraActionPerformed

    private void btn_guardarMateriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarMateriaActionPerformed
        if(txtF_nombreMateria.getText().equals("")){
           JOptionPane.showMessageDialog(rootPane, "¡Debes introducir un nombre!");
        }else{
            baseDatosMateria.guardar(txtF_nombreMateria.getText(), txtF_descripcionMateria.getText());
            JOptionPane.showMessageDialog(rootPane, "¡Guardado con Éxito!");
            vaciarMateria();
            cargarDatos();
        }
    }//GEN-LAST:event_btn_guardarMateriaActionPerformed

    private void btn_buscarCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarCarreraActionPerformed
        if(txtF_nombreCarrera.getText().equals("")){
           JOptionPane.showMessageDialog(rootPane, "¡Debes introducir un nombre!");
        }else{
            String valor = txtF_nombreCarrera.getText();
            String parametro = "nombre";
            mostrarDatosCarrera(parametro,valor);
        } 
    }//GEN-LAST:event_btn_buscarCarreraActionPerformed

    private void btn_buscarMateriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarMateriaActionPerformed
         if(txtF_nombreMateria.getText().equals("")){
           JOptionPane.showMessageDialog(rootPane, "¡Debes introducir un nombre!");
        }else{
            String valor = txtF_nombreMateria.getText();
            String parametro = "nombre";
            mostrarDatosMateria(parametro,valor);
        }   
    }//GEN-LAST:event_btn_buscarMateriaActionPerformed

    private void btn_cancelarCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarCarreraActionPerformed
        if(btn_guardarCarrera.isEnabled()){
            vaciarCarrera();
            mostrarDatosCarrera("","");
        }else{
            btn_editarCarrera.setEnabled(false);
            btn_guardarCarrera.setEnabled(true);
            btn_buscarCarrera.setEnabled(true);
            vaciarCarrera();
            mostrarDatosCarrera("","");
            idEditarCarrera = 0;            
        }
    }//GEN-LAST:event_btn_cancelarCarreraActionPerformed

    private void btn_cancelarMateriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarMateriaActionPerformed
        if(btn_guardarMateria.isEnabled()){
            vaciarMateria();
            mostrarDatosMateria("","");
        }else{
            btn_editarMateria.setEnabled(false);
            btn_guardarMateria.setEnabled(true);
            btn_buscarMateria.setEnabled(true);
            vaciarMateria();
            mostrarDatosMateria("","");
            idEditarMateria = 0; 
        }
    }//GEN-LAST:event_btn_cancelarMateriaActionPerformed

    private void menuIt_editarCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_editarCarreraActionPerformed
        int filaSeleccionada = table_carrera.getSelectedRow();
        
        idEditarCarrera = Integer.parseInt(table_carrera.getModel().getValueAt(filaSeleccionada, 1).toString());
 
        txtF_nombreCarrera.setText(table_carrera.getValueAt(filaSeleccionada, 1).toString().trim());
        txtF_descripcionCarrera.setText(table_carrera.getValueAt(filaSeleccionada, 2).toString().trim());
       
        btn_editarCarrera.setEnabled(true);
        btn_guardarCarrera.setEnabled(false);
        btn_buscarCarrera.setEnabled(false);
    }//GEN-LAST:event_menuIt_editarCarreraActionPerformed

    private void menuIt_editarMateriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_editarMateriaActionPerformed
        int filaSeleccionada = table_materia.getSelectedRow();
        
        idEditarMateria = Integer.parseInt(table_materia.getModel().getValueAt(filaSeleccionada, 1).toString());
 
        txtF_nombreMateria.setText(table_materia.getValueAt(filaSeleccionada, 1).toString().trim());
        txtF_descripcionMateria.setText(table_materia.getValueAt(filaSeleccionada, 2).toString().trim());
       
        btn_editarMateria.setEnabled(true);
        btn_guardarMateria.setEnabled(false);
        btn_buscarMateria.setEnabled(false);
    }//GEN-LAST:event_menuIt_editarMateriaActionPerformed

    private void menuIt_borrarCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_borrarCarreraActionPerformed
        int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Alerta!", JOptionPane.YES_NO_OPTION);
        
        if(resp==0){
            int filaSeleccionada = table_carrera.getSelectedRow();
            idEditarCarrera = Integer.parseInt(table_carrera.getModel().getValueAt(filaSeleccionada, 1).toString());
            baseDatosCarrera.delete(idEditarCarrera);
            JOptionPane.showMessageDialog(rootPane, "Borrado con Éxito!");
            mostrarDatosCarrera("","");
            idEditarCarrera = 0; 
        }
    }//GEN-LAST:event_menuIt_borrarCarreraActionPerformed

    private void menuIt_borrarMateriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_borrarMateriaActionPerformed
       int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Alerta!", JOptionPane.YES_NO_OPTION);
        
        if(resp==0){
            int filaSeleccionada = table_materia.getSelectedRow();
            idEditarMateria = Integer.parseInt(table_materia.getModel().getValueAt(filaSeleccionada, 1).toString());
            baseDatosMateria.delete(idEditarMateria);
            JOptionPane.showMessageDialog(rootPane, "Borrado con Éxito!");
            mostrarDatosMateria("","");
            idEditarMateria = 0; 
        }
    }//GEN-LAST:event_menuIt_borrarMateriaActionPerformed

    private void btn_editarMateriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarMateriaActionPerformed
        baseDatosMateria.actualizar(idEditarMateria, txtF_nombreMateria.getText(),txtF_descripcionMateria.getText());
        
        vaciarMateria();
        mostrarDatosMateria("","");
        
        JOptionPane.showMessageDialog(rootPane, "Editado con Éxito!");
        
        btn_editarMateria.setEnabled(false);
        btn_guardarMateria.setEnabled(true);
        btn_buscarMateria.setEnabled(true);
    }//GEN-LAST:event_btn_editarMateriaActionPerformed

    private void btn_guardarMateriaCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarMateriaCarreraActionPerformed
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date dateI = dateC_fechaInicio.getDate();
        Date dateF = dateC_fechaFin.getDate();
        String inicio = dateFormat.format(dateI);                                                                                   
        String fin = dateFormat.format(dateF);
                                                                                    
        Carrera c = (Carrera) comboB_carrera.getSelectedItem();
        Materia m = (Materia) comboB_materia.getSelectedItem();
        Docente d = (Docente) comboB_docente.getSelectedItem();
        
                                      
        baseDatosMateriaCarrera.guardar(inicio, fin, txtF_hsSemana.getText(), c.getId(), d.getId(), m.getId());
        mostrarDatosMateriasCarreras("","");
        vaciarMateriaCarrera();
    }//GEN-LAST:event_btn_guardarMateriaCarreraActionPerformed

    private void btn_buscarMateriaCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarMateriaCarreraActionPerformed
        mostrarDatosMateriasCarreras(comboB_busqueda.getSelectedItem().toString(), txtF_busqueda.getText());
    }//GEN-LAST:event_btn_buscarMateriaCarreraActionPerformed

    private void txtF_busquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtF_busquedaActionPerformed
        btn_buscarMateriaCarreraActionPerformed(evt);
    }//GEN-LAST:event_txtF_busquedaActionPerformed

    private void menuIt_editarMateriaCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_editarMateriaCarreraActionPerformed
        int filaSeleccionada = table_materiaCarrera.getSelectedRow();
        
        idEditarMateriaCarrera = Integer.parseInt(table_materiaCarrera.getModel().getValueAt(filaSeleccionada, 1).toString());
        
        int idC = Integer.parseInt(table_materiaCarrera.getModel().getValueAt(filaSeleccionada, 6).toString());
        int idM = Integer.parseInt(table_materiaCarrera.getModel().getValueAt(filaSeleccionada, 2).toString());
        int idD = Integer.parseInt(table_materiaCarrera.getModel().getValueAt(filaSeleccionada, 4).toString());
     
        for (int i = 0; i < comboB_carrera.getItemCount(); i++) {
            if(comboB_carrera.getItemAt(i).getId() == idC){
                comboB_carrera.setSelectedIndex(i);
            }
        }
        
        for (int i = 0; i < comboB_materia.getItemCount(); i++) {
            if(comboB_materia.getItemAt(i).getId() == idM){
                comboB_materia.setSelectedIndex(i);
            }
        }
        for (int i = 0; i < comboB_docente.getItemCount(); i++) {
            if(comboB_docente.getItemAt(i).getId() == idD){
                comboB_docente.setSelectedIndex(i);
            }
        }
        
        txtF_hsSemana.setText(table_materiaCarrera.getValueAt(filaSeleccionada, 4).toString().trim());
        
        String fecha_i = table_materiaCarrera.getValueAt(filaSeleccionada, 5).toString().trim();
        String fecha_f = table_materiaCarrera.getValueAt(filaSeleccionada, 6).toString().trim();
        
        int ano = Integer.parseInt(fecha_i.substring(0, 4))-2000+100;
        int mes = Integer.parseInt(fecha_i.substring(5, 7))-1;
        int dia = Integer.parseInt(fecha_i.substring(8, 10));
        
        dateC_fechaInicio.setDate(new Date(ano,mes,dia));
        
        ano = Integer.parseInt(fecha_f.substring(0, 4))-2000+100;
        mes = Integer.parseInt(fecha_f.substring(5, 7))-1;
        dia = Integer.parseInt(fecha_f.substring(8, 10));
        
        dateC_fechaFin.setDate(new Date(ano, mes, dia));
        
        btn_editarMateriaCarrera.setEnabled(true);
        btn_guardarMateriaCarrera.setEnabled(false);
        
    }//GEN-LAST:event_menuIt_editarMateriaCarreraActionPerformed

    private void menuIt_borrarMateriaCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuIt_borrarMateriaCarreraActionPerformed
        int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Alerta!", JOptionPane.YES_NO_OPTION);
        if(resp==0){
            int filaSeleccionada = table_materiaCarrera.getSelectedRow();
            idEditarMateriaCarrera = Integer.parseInt(table_materiaCarrera.getModel().getValueAt(filaSeleccionada, 1).toString());
            baseDatosMateriaCarrera.delete(idEditarMateriaCarrera);
            JOptionPane.showMessageDialog(rootPane, "Borrado con Éxito!");
            mostrarDatosMateriasCarreras("","");
            idEditarMateriaCarrera = 0; 
        }
    }//GEN-LAST:event_menuIt_borrarMateriaCarreraActionPerformed

    private void btn_editarCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarCarreraActionPerformed
        baseDatosCarrera.actualizar(idEditarCarrera, txtF_nombreCarrera.getText(),txtF_descripcionCarrera.getText());

        vaciarCarrera();
        mostrarDatosCarrera("","");

        JOptionPane.showMessageDialog(rootPane, "Editado con Éxito!");

        btn_editarCarrera.setEnabled(false);
        btn_guardarCarrera.setEnabled(true);
        btn_buscarCarrera.setEnabled(true);
    }//GEN-LAST:event_btn_editarCarreraActionPerformed

    private void btn_editarMateriaCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarMateriaCarreraActionPerformed
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date dateI = dateC_fechaInicio.getDate();
        Date dateF = dateC_fechaFin.getDate();
        String inicio = dateFormat.format(dateI);
        String fin = dateFormat.format(dateF);

        Carrera c = (Carrera) comboB_carrera.getSelectedItem();
        Materia m = (Materia) comboB_materia.getSelectedItem();
        Docente d = (Docente) comboB_docente.getSelectedItem();

        baseDatosMateriaCarrera.actualizar(idEditarMateriaCarrera, inicio, fin, txtF_hsSemana.getText(), c.getId(), d.getId(), m.getId());
        mostrarDatosMateriasCarreras("","");
        vaciarMateriaCarrera();
        idEditarMateriaCarrera = 0;
        JOptionPane.showMessageDialog(rootPane, "Editado con Éxito!");
        btn_editarMateriaCarrera.setEnabled(false);
        btn_guardarMateriaCarrera.setEnabled(true);
    }//GEN-LAST:event_btn_editarMateriaCarreraActionPerformed

    private void btn_cancelarMateriaCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarMateriaCarreraActionPerformed
        if(btn_guardarMateriaCarrera.isEnabled()){
            vaciarMateriaCarrera();
            mostrarDatosMateriasCarreras("","");
        }else{
            btn_editarMateriaCarrera.setEnabled(false);
            btn_guardarMateriaCarrera.setEnabled(true);
            vaciarMateriaCarrera();
            mostrarDatosMateriasCarreras("","");
            idEditarMateriaCarrera = 0;
        }
    }//GEN-LAST:event_btn_cancelarMateriaCarreraActionPerformed
    
    
    //Funciones y Procedimiento Propios
    
    private void cargarDatos(){
        mostrarDatosCarrera("","");
        mostrarDatosMateria("","");
        mostrarDatosMateriasCarreras("","");
        mostrarListaCarreras();
        mostrarListaMaterias();
        mostrarListaDocentes();
    }
    
    private void mostrarDatosCarrera(String parametro, String valor){
        
        btn_editarCarrera.setEnabled(false);                           //esconder el boton editar
        
        DefaultTableModel modelo = new DefaultTableModel();     //establecer modelo de la tabla
        modelo.addColumn("Contador");
        modelo.addColumn("Id");
        modelo.addColumn("Nombre");
        modelo.addColumn("Descripcion");
        String[] datos = new String[4];                         //cargar los datos a la tabla
        
        try{
            
            ResultSet rs = baseDatosCarrera.buscarPor(parametro, valor);
            int i = 1;
        
            while(rs.next()){
                
                datos[0] = String.valueOf(i);                   //Contador
                datos[1] = rs.getString(1);                     //Id
                datos[2] = rs.getString(2);                     //Nombre
                datos[3] = rs.getString(3);                     //Descripcion
           
                modelo.addRow(datos);
                i++;
            }
            
            table_carrera.setModel(modelo);
            TableColumnModel tcm = table_carrera.getColumnModel();
            tcm.removeColumn(tcm.getColumn(1));
            
        }catch(SQLException ex){
            System.out.println("Algo falló"+ex);
        }
    }
    
    private void mostrarDatosMateria(String parametro, String valor){
        
        btn_editarMateria.setEnabled(false);                           //esconder el boton editar
        
        DefaultTableModel modelo = new DefaultTableModel();     //establecer modelo de la tabla
        modelo.addColumn("Contador");
        modelo.addColumn("Id");
        modelo.addColumn("Nombre");
        modelo.addColumn("Descripcion");
        String[] datos = new String[4];                         //cargar los datos a la tabla
        
        try{
            
            ResultSet rs = baseDatosMateria.buscarPor(parametro, valor);
            int i = 1;
        
            while(rs.next()){
                
                datos[0] = String.valueOf(i);                   //Contador
                datos[1] = rs.getString(1);                     //Id
                datos[2] = rs.getString(2);                     //Nombre
                datos[3] = rs.getString(3);                     //Descripcion
           
                modelo.addRow(datos);
                i++;
            }
            
            table_materia.setModel(modelo);
            TableColumnModel tcm = table_materia.getColumnModel();
            tcm.removeColumn(tcm.getColumn(1));
            
        }catch(SQLException ex){
            System.out.println("Algo falló"+ex);
        }
    }
    
    public void mostrarDatosMateriasCarreras(String parametro, String valor){
        btn_editarMateriaCarrera.setEnabled(false);
        
        DefaultTableModel modelo = new DefaultTableModel();     //establecer modelo de la tabla
        modelo.addColumn("Contador");
        modelo.addColumn("IdMateriaPeriodo");
        modelo.addColumn("IdMateria");
        modelo.addColumn("Materia");
        modelo.addColumn("IdDocente");
        modelo.addColumn("Docente");
        modelo.addColumn("IdCarrera");
        modelo.addColumn("Carrera");
        modelo.addColumn("Hs/S");
        modelo.addColumn("Inicio");
        modelo.addColumn("Fin");
        modelo.addColumn("Estado");
   
        String[] datos = new String[12];
        
        try{
            
            ResultSet rs = baseDatosMateriaCarrera.buscarPor(parametro, valor);
            int i = 1;
            
            while(rs.next()){
                
                datos[0] = String.valueOf(i);                         //Contador
                datos[1] = rs.getString(1);                           //IdMateriaPeriodo
                datos[2] = rs.getString(2);                           //IdMateria
                datos[3] = rs.getString(3);                           //Materia
                datos[4] = rs.getString(4);                           //IdDocente
                datos[5] = rs.getString(5) + " " + rs.getString(6);   //Nombre Docente + Apellido Docente
                datos[6] = rs.getString(7);                           //IdCarrera
                datos[7] = rs.getString(8);                           //Carrera
                datos[8] = rs.getString(9);                           //Hs/S
                datos[9] = rs.getString(10);                          //Inicio
                datos[10] = rs.getString(11);                         //Fin
                datos[11] = rs.getString(12);                         //Estado Materia
                
                modelo.addRow(datos);
                i++;
            }
            
            table_materiaCarrera.setModel(modelo);
            TableColumnModel tcm = table_materiaCarrera.getColumnModel();
            
            tcm.removeColumn(tcm.getColumn(1)); //IdMateriaPeriodo
            tcm.removeColumn(tcm.getColumn(1)); //IdMateria
            tcm.removeColumn(tcm.getColumn(2)); //IdDocente
            tcm.removeColumn(tcm.getColumn(3)); //IdCarrera
            
        }catch(SQLException ex){
            System.out.println("Algo falló"+ex);
        }
        
    }
    
    private void mostrarListaCarreras(){
        comboB_carrera.removeAllItems();
        listaCarreras = baseDatosCarrera.getListaCarreras();
        Iterator iterador = listaCarreras.iterator();
        while(iterador.hasNext()){
            Carrera carrera = (Carrera) iterador.next();
            comboB_carrera.addItem(carrera);
        }
        AutoCompleteDecorator.decorate(comboB_carrera); 
    }
    
    private void mostrarListaMaterias(){
        comboB_materia.removeAllItems();
        listaMaterias = baseDatosMateria.getListaMaterias();
        Iterator iterador = listaMaterias.iterator();
        while(iterador.hasNext()){
            Materia mater = (Materia) iterador.next();
            comboB_materia.addItem(mater);
        }
        AutoCompleteDecorator.decorate(comboB_materia); 
    }
    
    private void mostrarListaDocentes(){
        comboB_docente.removeAllItems();
        listaDocentes = baseDatosDocentes.getListaDocentes();
        Iterator iterador = listaDocentes.iterator();
        while(iterador.hasNext()){
            Docente docen = (Docente) iterador.next();
            comboB_docente.addItem(docen);
        }
        AutoCompleteDecorator.decorate(comboB_docente); 
    }
    
    private void vaciarCarrera(){
        txtF_nombreCarrera.setText("");
        txtF_descripcionCarrera.setText("");
    }
    
    private void vaciarMateria(){
        txtF_nombreMateria.setText("");
        txtF_descripcionMateria.setText("");
    }
    
    private void vaciarMateriaCarrera(){
        comboB_carrera.setSelectedIndex(0);
        comboB_materia.setSelectedIndex(0);
        comboB_docente.setSelectedIndex(0);
        txtF_hsSemana.setText("");
        dateC_fechaInicio.setDate(null);
        dateC_fechaFin.setDate(null);
    }
    
    //Main
    public static void main(String args[]) {
        //try{
        //   UIManager.setLookAndFeel(newLookAndFeel);
        //}catch(Exception e){
          //  e.printStackTrace();
        //} 
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ABM_Carreras_Materias().setVisible(true);
            }
        });
    }
    
    //Variables Propias
    private ConexionCarreras baseDatosCarrera = new ConexionCarreras();
    private ConexionMaterias baseDatosMateria = new ConexionMaterias();
    private ConexionDocentes baseDatosDocentes = new ConexionDocentes();
    private ConexionMateriasCarreras baseDatosMateriaCarrera = new ConexionMateriasCarreras();
    private ArrayList listaCarreras;
    private ArrayList<Materia> listaMaterias;
    private ArrayList listaDocentes;
    private Integer idEditarCarrera= 0;
    private Integer idEditarMateria = 0;
    private Integer idEditarMateriaCarrera = 0;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_buscarCarrera;
    private javax.swing.JButton btn_buscarMateria;
    private javax.swing.JButton btn_buscarMateriaCarrera;
    private javax.swing.JButton btn_cancelarCarrera;
    private javax.swing.JButton btn_cancelarMateria;
    private javax.swing.JButton btn_cancelarMateriaCarrera;
    private javax.swing.JButton btn_editarCarrera;
    private javax.swing.JButton btn_editarMateria;
    private javax.swing.JButton btn_editarMateriaCarrera;
    private javax.swing.JButton btn_guardarCarrera;
    private javax.swing.JButton btn_guardarMateria;
    private javax.swing.JButton btn_guardarMateriaCarrera;
    private javax.swing.JComboBox<String> comboB_busqueda;
    private javax.swing.JComboBox<Carrera> comboB_carrera;
    private javax.swing.JComboBox<Docente> comboB_docente;
    private javax.swing.JComboBox<Materia> comboB_materia;
    private com.toedter.calendar.JDateChooser dateC_fechaFin;
    private com.toedter.calendar.JDateChooser dateC_fechaInicio;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel labelD_carreraMateria;
    private javax.swing.JLabel labelD_decripcionCarrera;
    private javax.swing.JLabel labelD_descripcionMateria;
    private javax.swing.JLabel labelD_docenteMateria;
    private javax.swing.JLabel labelD_fechaFin;
    private javax.swing.JLabel labelD_fechaInicio;
    private javax.swing.JLabel labelD_hsSemana;
    private javax.swing.JLabel labelD_materiaCarrera;
    private javax.swing.JLabel labelD_nombreCarrera;
    private javax.swing.JLabel labelD_nombreMateria;
    private javax.swing.JLabel labelT_carrera;
    private javax.swing.JLabel labelT_materia;
    private javax.swing.JLabel labelT_materiaCarrera;
    private javax.swing.JMenuItem menuIt_borrarCarrera;
    private javax.swing.JMenuItem menuIt_borrarMateria;
    private javax.swing.JMenuItem menuIt_borrarMateriaCarrera;
    private javax.swing.JMenuItem menuIt_editarCarrera;
    private javax.swing.JMenuItem menuIt_editarMateria;
    private javax.swing.JMenuItem menuIt_editarMateriaCarrera;
    private javax.swing.JPopupMenu menu_carrera;
    private javax.swing.JPopupMenu menu_materia;
    private javax.swing.JPopupMenu menu_materiaCarrera;
    private javax.swing.JPanel panel_contenedor;
    private javax.swing.JTable table_carrera;
    private javax.swing.JTable table_materia;
    private javax.swing.JTable table_materiaCarrera;
    private javax.swing.JTextField txtF_busqueda;
    private javax.swing.JTextField txtF_descripcionCarrera;
    private javax.swing.JTextField txtF_descripcionMateria;
    private javax.swing.JTextField txtF_hsSemana;
    private javax.swing.JTextField txtF_nombreCarrera;
    private javax.swing.JTextField txtF_nombreMateria;
    // End of variables declaration//GEN-END:variables
}
