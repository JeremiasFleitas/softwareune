-- MySQL Script generated by MySQL Workbench
-- Fri Oct 22 19:59:34 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`alumnos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`alumnos` (
  `id_alumno` INT NULL AUTO_INCREMENT,
  `nombre_alumno` VARCHAR(45) NULL,
  `apellido_alumno` VARCHAR(45) NULL,
  `documento_alumno` VARCHAR(45) NULL,
  `tipo_doc_alumno` VARCHAR(45) NULL,
  `contacto_alumno` VARCHAR(45) NULL,
  `email_alumno` VARCHAR(45) NULL,
  PRIMARY KEY (`id_alumno`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`docentes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`docentes` (
  `id_docentes` INT NOT NULL AUTO_INCREMENT,
  `nombre_docente` VARCHAR(45) NULL,
  `apellido_docente` VARCHAR(45) NULL,
  `documento_docente` VARCHAR(45) NULL,
  `tipo_doc_docente` VARCHAR(45) NULL,
  `contacto_docente` VARCHAR(45) NULL,
  `email_docente` VARCHAR(45) NULL,
  PRIMARY KEY (`id_docentes`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`pagos_alumnos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`pagos_alumnos` (
  `id_pago_alumno` INT NOT NULL AUTO_INCREMENT,
  `monto_pago_alumno` VARCHAR(45) NULL,
  `concepto_pago_alumno` VARCHAR(45) NULL,
  `created_at` VARCHAR(45) NULL,
  `update_at` VARCHAR(45) NULL,
  `id_alumno` INT NOT NULL,
  PRIMARY KEY (`id_pago_alumno`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`acreditaciones_docentes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`acreditaciones_docentes` (
  `id_titulos_docente` INT NOT NULL AUTO_INCREMENT,
  `titulo_docente` VARCHAR(45) NULL,
  `area_docente` VARCHAR(45) NULL,
  `id_docente` INT NOT NULL,
  PRIMARY KEY (`id_titulos_docente`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`carreras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`carreras` (
  `id_carrera` INT NOT NULL AUTO_INCREMENT,
  `nombre_carrera` VARCHAR(45) NULL,
  PRIMARY KEY (`id_carrera`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`alumnos_carreras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`alumnos_carreras` (
  `id_alumnos_carrera` INT NOT NULL,
  `estado` VARCHAR(45) NULL,
  `id_alumno` INT NOT NULL,
  `id_carrera` INT NOT NULL,
  PRIMARY KEY (`id_alumnos_carrera`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`materias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`materias` (
  `id_materia` INT NOT NULL AUTO_INCREMENT,
  `nombre_materia` VARCHAR(45) NULL,
  `plan` VARCHAR(45) NULL,
  `estado` VARCHAR(45) NULL,
  PRIMARY KEY (`id_materia`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`materias_periodo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`materias_periodo` (
  `id_materia_periodo` INT NOT NULL AUTO_INCREMENT,
  `semestre` INT NULL,
  `anho` INT NULL,
  `id_materia` INT NOT NULL,
  `id_docentes` INT NOT NULL,
  `id_carrera` INT NOT NULL,
  PRIMARY KEY (`id_materia_periodo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`alumnos_materias_periodo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`alumnos_materias_periodo` (
  `id_alumno_materia` INT NOT NULL AUTO_INCREMENT,
  `estado_alumno_materia` VARCHAR(45) NULL,
  `id_alumno` INT NOT NULL,
  `id_materia_periodo` INT NOT NULL,
  PRIMARY KEY (`id_alumno_materia`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`asistencias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`asistencias` (
  `id_asistencia` INT NOT NULL AUTO_INCREMENT,
  `fecha_asistencia` VARCHAR(45) NULL,
  `presencia_asistencia` VARCHAR(45) NULL,
  `id_alumno_materia_periodo` INT NOT NULL,
  PRIMARY KEY (`id_asistencia`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
